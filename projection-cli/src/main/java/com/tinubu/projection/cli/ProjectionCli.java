/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.cli;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.lang.System.exit;
import static picocli.CommandLine.Command;
import static picocli.CommandLine.Model.CommandSpec;
import static picocli.CommandLine.ParentCommand;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.projection.cli.ProjectionCli.Synchronize;
import com.tinubu.projection.cli.ProjectionCli.Update;
import com.tinubu.projection.core.Attribute;
import com.tinubu.projection.core.Projection;
import com.tinubu.projection.core.Projection.ProjectionSynchronize;
import com.tinubu.projection.core.Projection.ProjectionUpdate;
import com.tinubu.projection.core.strategy.ServiceLoaderSynchronizeStrategyFactory;

import picocli.CommandLine;
import picocli.CommandLine.Help.Ansi.Style;
import picocli.CommandLine.Help.ColorScheme;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Spec;

@Command(name = "projection", description = "Project template synchronization", version = {
      "projection ${git.build.version:--} (${git.build.time:--})",
      "Git: ${git.commit.id.abbrev:--} (${git.commit.time:--})",
      "JVM: ${java.version} (${java.vendor} ${java.vm.name} ${java.vm.version})" },
         mixinStandardHelpOptions = true, sortOptions = false, parameterListHeading = "\n",
         optionListHeading = "\n", commandListHeading = "%nCommands:%n",
         subcommands = { Synchronize.class, Update.class })
public class ProjectionCli implements Runnable {

   private static final Logger log = LoggerFactory.getLogger(ProjectionCli.class);

   private static final int USAGE_HELP_WIDTH = 120;
   /** Projection application common root for all logs. */
   private static final String PROJECTION_LOG_ROOT = "com.tinubu.projection";

   static {
      ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
   }

   @Spec
   CommandSpec spec;

   public static void main(String[] args) {
      exit(new ProjectionCli().commandLine(args));
   }

   public int commandLine(String[] args) {
      loadGitProperties();

      CommandLine commandLine = new CommandLine(this);
      commandLine.setToggleBooleanFlags(false);
      commandLine.setCaseInsensitiveEnumValuesAllowed(true);
      commandLine.setUsageHelpWidth(USAGE_HELP_WIDTH);
      commandLine.setColorScheme(colorScheme());

      return commandLine.execute(args);
   }

   @Override
   public void run() {
      spec.commandLine().usage(System.out);
   }

   @Command(name = "synchronize", aliases = { "sync" }, description = "Synchronize project from template",
            mixinStandardHelpOptions = true, sortOptions = false, parameterListHeading = "\n",
            optionListHeading = "\n")
   public static class Synchronize implements Runnable {

      @ParentCommand
      ProjectionCli projection;

      @Parameters(description = "Project URI [${DEFAULT-VALUE}]", paramLabel = "<project URI>",
                  defaultValue = ".")
      private URI projectUri;

      @Option(names = { "-t", "--template" },
              description = "Template URI [referenced templateUri from project configuration]",
              paramLabel = "<template URI>")
      private URI templateUri;

      @Option(names = { "-O", "--overwrite" }, description = "Overwrite documents [${DEFAULT-VALUE}]",
              defaultValue = "false")
      private boolean overwrite;

      @Option(names = { "--infer-placeholders" },
              description = "Replace inferred project attribute values placeholders [${DEFAULT-VALUE}]",
              defaultValue = "false")
      private boolean inferPlaceholders;

      @Option(names = { "--include-git" },
              description = "Include template .git/ directory in synchronization [${DEFAULT-VALUE}]",
              defaultValue = "false")
      private boolean includeGit;

      @Option(names = { "--generate-configuration" },
              description = "Generate projection configuration [${DEFAULT-VALUE}]", defaultValue = "true")
      private boolean generateProjectionConfiguration;

      @Option(names = { "--generate-metadata" },
              description = "Generate projection metadata [${DEFAULT-VALUE}]", defaultValue = "true")
      private boolean generateProjectionMetadata;

      @Option(names = { "--strategy" }, description = "Synchronize strategy [${DEFAULT-VALUE}]",
              paramLabel = "<strategy>", defaultValue = "direct")
      private String synchronizeStrategy;

      @Option(names = { "-A" }, description = "Template attributes. Required attributes are suffixed with *",
              paramLabel = "<name>[*]=<value>")
      private Map<String, String> templateAttributes = map();

      @Option(names = { "-a" }, description = "Project attributes. Required attributes are suffixed with *",
              paramLabel = "<name>[*]=<value>")
      private Map<String, String> projectAttributes = map();

      @Option(names = { "-D" },
              description = "Global properties. Sensitive properties can be suffixed with !",
              paramLabel = "<name>[!]=<value>", mapFallbackValue = "true")
      private Map<String, String> properties = map();

      @Option(names = { "-v", "--verbose" }, description = "Verbose level [${DEFAULT-VALUE}]",
              defaultValue = "warn", arity = "0..1", fallbackValue = "info")
      private ExtendedLogger.Level verbose;

      @Override
      public void run() {
         verboseLevel(verbose);

         try {
            Projection projection = new Projection().properties(properties);

            try (ProjectionSynchronize synchronize = projection.synchronize()) {
               synchronize
                     .projectRepository(projectUri)
                     .templateAttributes(attributes(templateAttributes))
                     .projectAttributes(attributes(projectAttributes))
                     .overwrite(overwrite)
                     .inferPlaceholders(inferPlaceholders)
                     .excludeGit(!includeGit)
                     .generateProjectionConfiguration(generateProjectionConfiguration)
                     .generateProjectionMetadata(generateProjectionMetadata)
                     .synchronizeStrategyFactory(new ServiceLoaderSynchronizeStrategyFactory(
                           synchronizeStrategy));
               if (templateUri != null) {
                  synchronize.templateRepository(templateUri);
               }

               synchronize.synchronize();
            }
         } catch (Exception e) {
            ProjectionCli.generalError(e);
         }
      }

   }

   @Command(name = "update", description = "Update template from project selected documents",
            mixinStandardHelpOptions = true, sortOptions = false, parameterListHeading = "\n",
            optionListHeading = "\n")
   public static class Update implements Runnable {

      @ParentCommand
      ProjectionCli projection;

      @Parameters(description = "Project URI [${DEFAULT-VALUE}]", paramLabel = "<project URI>",
                  defaultValue = ".", index = "0")
      private URI projectUri;

      @Parameters(description = "Document paths (relative to project)", paramLabel = "<path>", arity = "1",
                  index = "1..*")
      private List<Path> documents;

      @Option(names = { "-t", "--template" },
              description = "Template URI [referenced templateUri from project configuration]",
              paramLabel = "<template URI>")
      private URI templateUri;

      @Option(names = { "-O", "--overwrite" }, description = "Overwrite documents [${DEFAULT-VALUE}]",
              defaultValue = "false")
      private boolean overwrite;

      @Option(names = { "--infer-placeholders" },
              description = "Replace inferred project attribute values placeholders [${DEFAULT-VALUE}]",
              defaultValue = "false")
      private boolean inferPlaceholders;

      @Option(names = { "-A" }, description = "Template attributes. Required attributes are suffixed with *",
              paramLabel = "<name>[*]=<value>")
      private Map<String, String> templateAttributes = map();

      @Option(names = { "-a" }, description = "Project attributes. Required attributes are suffixed with *",
              paramLabel = "<name>[*]=<value>")
      private Map<String, String> projectAttributes = map();

      @Option(names = { "-D" },
              description = "Global properties. Sensitive properties can be suffixed with !",
              paramLabel = "<name>[!]=<value>", mapFallbackValue = "true")
      private Map<String, String> properties = map();

      @Option(names = { "-v", "--verbose" }, description = "Verbose level [${DEFAULT-VALUE}]",
              defaultValue = "warn", arity = "0..1", fallbackValue = "info")
      private ExtendedLogger.Level verbose;

      @Override
      public void run() {
         verboseLevel(verbose);

         try {
            Projection projection = new Projection().properties(properties);

            try (ProjectionUpdate update = projection.update()) {
               update
                     .projectRepository(projectUri)
                     .templateAttributes(attributes(templateAttributes))
                     .projectAttributes(attributes(projectAttributes))
                     .overwrite(overwrite)
                     .inferPlaceholders(inferPlaceholders);
               if (templateUri != null) {
                  update.templateRepository(templateUri);
               }

               update.update(list(stream(documents).map(DocumentPath::of)));
            }
         } catch (Exception e) {
            ProjectionCli.generalError(e);
         }
      }

   }

   /**
    * Loads {@code git.properties} file entries as new system properties.
    */
   private static void loadGitProperties() {
      try (InputStream gitPropertiesStream = Thread
            .currentThread()
            .getContextClassLoader()
            .getResourceAsStream("git-projection.properties")) {
         if (gitPropertiesStream != null) {
            Properties gitProperties = new Properties();
            gitProperties.load(gitPropertiesStream);
            gitProperties.forEach((k, v) -> System.setProperty(k.toString(), v.toString()));
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Manages uncaught exception.
    *
    * @param e uncaught exception
    */
   public static void generalError(Exception e) {
      System.err.println("[E] " + e.getMessage());
      if (log.isDebugEnabled()) {
         log.debug(e.getMessage(), e);
      }
      exit(1);
   }

   private static List<Attribute> attributes(Map<String, String> attributes) {
      return list(map(attributes).entrySet().stream().map(e -> Attribute.of(e.getKey(), e.getValue())));
   }

   private ColorScheme colorScheme() {
      return new ColorScheme.Builder()
            .commands(Style.fg_green, Style.bold)
            .options(Style.fg_cyan)
            .parameters(Style.fg_cyan)
            .optionParams(Style.italic)
            .errors(Style.fg_red, Style.bold)
            .stackTraces(Style.italic)
            .build();
   }

   public static void verboseLevel(ExtendedLogger.Level level) {
      ExtendedLogger.setLoggerLevel(PROJECTION_LOG_ROOT, level);
   }

}

