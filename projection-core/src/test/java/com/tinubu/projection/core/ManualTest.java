/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static org.assertj.core.util.Lists.list;

import java.nio.file.Path;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.memory.MemoryDocumentRepository;
import com.tinubu.projection.core.Projection.ProjectionSynchronize;
import com.tinubu.projection.core.Projection.ProjectionUpdate;
import com.tinubu.projection.core.config.ConfigurableProjectionConfiguration;
import com.tinubu.projection.core.config.ProjectionConfiguration.BiDirectionalFilter;
import com.tinubu.projection.core.config.ProjectionConfiguration.Filter;
import com.tinubu.projection.core.strategy.DirectSynchronizeStrategy.DirectSynchronizeStrategyFactory;

@Disabled("Development manual tests")
public class ManualTest {

   @Test
   public void synchronize() {
      Projection projection = new Projection().properties(CollectionUtils.map(Pair.of(
            "projection.repository.git.transport.disable-ssl-verify",
            "true")));

      try (ProjectionSynchronize synchronize = projection.synchronize()) {
         synchronize
               .templateRepository("https://gitlab.tinubu.local/tinubu/audit.git#master")
               .templateConfiguration(new ConfigurableProjectionConfiguration.Builder()
                                            .synchronizeIncludeExclude(new BiDirectionalFilter(new Filter(list(),
                                                                                                          list(Path.of(
                                                                                                                "glob:.git/**")))))
                                            .build())
               .projectRepository(new MemoryDocumentRepository())
               //.projectRepository("file:/tmp/projection")
               .templateAttributes(Attribute.of("project", "audit"), Attribute.of("group", "com.tsquare"))
               .projectAttributes(Attribute.of("project", "template"), Attribute.of("group", "com.tsquare"))
               .inferPlaceholders(false)
               .overwrite(true)
               .synchronizeStrategyFactory(new DirectSynchronizeStrategyFactory())
               .synchronize();
      }
   }

   @Test
   public void update() {
      Projection projection = new Projection().properties(CollectionUtils.map(Pair.of(
            "projection.repository.git.transport.disable-ssl-verify",
            "true")));

      try (ProjectionUpdate update = projection.update()) {
         update.templateRepository("/tmp/test1").projectRepository("/tmp/test2")
               //.templateAttributes(Attribute.of("project", "test1"), Attribute.of("group", "com.tsquare"))
               //.projectAttributes(Attribute.of("project", "test2"), Attribute.of("group", "com.tsquare"))
               .inferPlaceholders(false).overwrite(true).update(DocumentPath.of("README.adoc"));
      }
   }
}
