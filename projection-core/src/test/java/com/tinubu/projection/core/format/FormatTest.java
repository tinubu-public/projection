/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.projection.core.Attribute;

class FormatTest {

   private String format(Format format, String value) {
      return format.format(Attribute.of("test", value), false);
   }

   @Test
   public void testLowerCase() {
      assertThat(format(new LowerCaseFormat(), "my-project")).isEqualTo("my-project");
      assertThat(format(new LowerCaseFormat(), "my project")).isEqualTo("my project");
      assertThat(format(new LowerCaseFormat(), "my_project")).isEqualTo("my_project");
      assertThat(format(new LowerCaseFormat(), "my.project")).isEqualTo("my.project");
      assertThat(format(new LowerCaseFormat(), "my!project")).isEqualTo("my!project");
      assertThat(format(new LowerCaseFormat(), "myProject")).isEqualTo("myproject");
      assertThat(format(new LowerCaseFormat(), "my._Project!")).isEqualTo("my._project!");
   }

   @Test
   public void testUpperCase() {
      assertThat(format(new UpperCaseFormat(), "my-project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperCaseFormat(), "my project")).isEqualTo("MY PROJECT");
      assertThat(format(new UpperCaseFormat(), "my_project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperCaseFormat(), "my.project")).isEqualTo("MY.PROJECT");
      assertThat(format(new UpperCaseFormat(), "my!project")).isEqualTo("MY!PROJECT");
      assertThat(format(new UpperCaseFormat(), "myProject")).isEqualTo("MYPROJECT");
      assertThat(format(new UpperCaseFormat(), "my._Project!")).isEqualTo("MY._PROJECT!");
   }

   @Test
   public void testCapitalize() {
      assertThat(format(new CapitalizeFormat(), "my-project")).isEqualTo("My-project");
      assertThat(format(new CapitalizeFormat(), "my project")).isEqualTo("My project");
      assertThat(format(new CapitalizeFormat(), "my_project")).isEqualTo("My_project");
      assertThat(format(new CapitalizeFormat(), "my.project")).isEqualTo("My.project");
      assertThat(format(new CapitalizeFormat(), "my!project")).isEqualTo("My!project");
      assertThat(format(new CapitalizeFormat(), "myProject")).isEqualTo("Myproject");
      assertThat(format(new CapitalizeFormat(), "my._Project!")).isEqualTo("My._project!");
      assertThat(format(new CapitalizeFormat(), "!my._Project!")).isEqualTo("!my._project!");
   }

   @Test
   public void testWordCapitalize() {
      assertThat(format(new WordCapitalizeFormat(), "my-project")).isEqualTo("My-Project");
      assertThat(format(new WordCapitalizeFormat(), "my project")).isEqualTo("My Project");
      assertThat(format(new WordCapitalizeFormat(), "my_project")).isEqualTo("My_Project");
      assertThat(format(new WordCapitalizeFormat(), "my.project")).isEqualTo("My.Project");
      assertThat(format(new WordCapitalizeFormat(), "my!project")).isEqualTo("My!project");
      // FIXME assertThat(format(new WordCapitalizeFormat(), "myProject"))).isEqualTo("MyProject");
      assertThat(format(new WordCapitalizeFormat(), ".my._Project!")).isEqualTo(".My._Project!");
      assertThat(format(new WordCapitalizeFormat(), "my.project!")).isEqualTo("My.Project!");
      assertThat(format(new WordCapitalizeFormat(), "my-project!")).isEqualTo("My-Project!");
      assertThat(format(new WordCapitalizeFormat(), "my_project!")).isEqualTo("My_Project!");
      assertThat(format(new WordCapitalizeFormat(), "my!project!")).isEqualTo("My!project!");
   }

   @Test
   public void testLowerCamlCase() {
      assertThat(format(new LowerCamelCaseFormat(), "my-project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "my project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "my_project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "my.project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "my!project")).isEqualTo("myProject");
      // FIXME assertThat(format(new LowerCamelCaseFormat()), "myProject"))).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "!my._Project!")).isEqualTo("myProject");
   }

   @Test
   public void testUpperCamlCase() {
      assertThat(format(new UpperCamelCaseFormat(), "my-project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "my project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "my_project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "my.project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "my!project")).isEqualTo("MyProject");
      // FIXME assertThat(format(new UpperCamelCaseFormat(), "myProject"))).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "!my._Project!")).isEqualTo("MyProject");
   }

   @Test
   public void testLowerSnakeCase() {
      assertThat(format(new LowerSnakeCaseFormat(), "my-project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "my project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "my_project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "my.project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "my!project")).isEqualTo("my_project");
      // FIXME assertThat(format(new LowerSnakeCaseFormat(), "myProject"))).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "!my._Project!")).isEqualTo("my_project");
   }

   @Test
   public void testUpperSnakeCase() {
      assertThat(format(new UpperSnakeCaseFormat(), "my-project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "my project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "my_project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "my.project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "my!project")).isEqualTo("MY_PROJECT");
      // FIXME assertThat(format(new UpperSnakeCaseFormat(), "myProject"))).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "!my._Project!")).isEqualTo("MY_PROJECT");
   }

   @Test
   public void testLowerKebabCase() {
      assertThat(format(new LowerKebabCaseFormat(), "my-project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "my project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "my_project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "my.project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "my!project")).isEqualTo("my-project");
      // FIXME assertThat(format(new LowerKebabCaseFormat(), "myProject"))).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "!my._Project!")).isEqualTo("my-project");
   }

   @Test
   public void testUpperKebabCase() {
      assertThat(format(new UpperKebabCaseFormat(), "my-project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "my project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "my_project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "my.project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "my!project")).isEqualTo("MY-PROJECT");
      // FIXME assertThat(format(new UpperKebabCaseFormat(), "myProject"))).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "!my._Project!")).isEqualTo("MY-PROJECT");
   }

}