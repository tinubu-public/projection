/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.api.ThrowingConsumer;
import org.junit.jupiter.api.Test;

import com.tinubu.projection.core.format.CapitalizeFormat;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.LowerCamelCaseFormat;
import com.tinubu.projection.core.format.LowerCaseFormat;
import com.tinubu.projection.core.format.LowerKebabCaseFormat;
import com.tinubu.projection.core.format.LowerNoSpecialFormat;
import com.tinubu.projection.core.format.LowerSnakeCaseFormat;
import com.tinubu.projection.core.format.UpperCamelCaseFormat;
import com.tinubu.projection.core.format.UpperCaseFormat;
import com.tinubu.projection.core.format.UpperKebabCaseFormat;
import com.tinubu.projection.core.format.UpperNoSpecialFormat;
import com.tinubu.projection.core.format.UpperSnakeCaseFormat;
import com.tinubu.projection.core.format.WordCapitalizeFormat;

class AttributeTest {

   private static final List<Format> FORMATS = list(new LowerCaseFormat(),
                                                    new UpperCaseFormat(),
                                                    new CapitalizeFormat(),
                                                    new WordCapitalizeFormat(),
                                                    new LowerCamelCaseFormat(),
                                                    new UpperCamelCaseFormat(),
                                                    new LowerNoSpecialFormat(),
                                                    new UpperNoSpecialFormat(),
                                                    new LowerSnakeCaseFormat(),
                                                    new UpperSnakeCaseFormat(),
                                                    new LowerKebabCaseFormat(),
                                                    new UpperKebabCaseFormat());

   @Test
   public void testAmbiguousFormats() {
      Attribute attribute1 = Attribute.of("com.group", "project");

      assertThat(attribute1.ambiguousFormats(FORMATS)).satisfiesExactlyInAnyOrder(matchFormats("lowercase",
                                                                                               "lowercamelcase",
                                                                                               "lowernospecial",
                                                                                               "lowersnakecase",
                                                                                               "lowerkebabcase"),
                                                                                  matchFormats("uppercase",
                                                                                               "uppernospecial",
                                                                                               "uppersnakecase",
                                                                                               "upperkebabcase"),
                                                                                  matchFormats("capitalize",
                                                                                               "wordcapitalize",
                                                                                               "uppercamelcase"));

      Attribute attribute2 = Attribute.of("com.group", "my-project");

      assertThat(attribute2.ambiguousFormats(FORMATS)).satisfiesExactlyInAnyOrder(matchFormats("lowercase",
                                                                                               "lowerkebabcase"),
                                                                                  matchFormats("uppercase",
                                                                                               "upperkebabcase"),
                                                                                  matchFormats("capitalize"),
                                                                                  matchFormats(
                                                                                        "wordcapitalize"),
                                                                                  matchFormats(
                                                                                        "lowercamelcase"),
                                                                                  matchFormats(
                                                                                        "uppercamelcase"),
                                                                                  matchFormats(
                                                                                        "lowernospecial"),
                                                                                  matchFormats(
                                                                                        "uppernospecial"),
                                                                                  matchFormats(
                                                                                        "lowersnakecase"),
                                                                                  matchFormats(
                                                                                        "uppersnakecase"));

   }

   private static ThrowingConsumer<List<Format>> matchFormats(String... groups) {
      return group -> assertThat(group).extracting(Format::name).containsExactly(groups);
   }
}