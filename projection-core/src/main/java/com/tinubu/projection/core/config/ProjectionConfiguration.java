/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.config;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoDuplicates;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullValues;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.keys;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.rules.CollectionRules;
import com.tinubu.commons.ddd2.invariant.rules.MapRules;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.projection.core.Attribute;

// FIXME multiple placeholders ? Structure to pair begin and end
public abstract class ProjectionConfiguration extends AbstractValue {
   private static final String DEFAULT_CONTENT_PLACEHOLDER_BEGIN = "$__";
   private static final String DEFAULT_CONTENT_PLACEHOLDER_END = "__";
   private static final String DEFAULT_CONTENT_PLACEHOLDER_FORMAT_SEPARATOR = "_";
   private static final String DEFAULT_PATH_PLACEHOLDER_BEGIN = "__";
   private static final String DEFAULT_PATH_PLACEHOLDER_END = "__";
   private static final String DEFAULT_PATH_PLACEHOLDER_FORMAT_SEPARATOR = "_";

   private final URI templateUri;
   private final Map<String, Attribute> attributes;
   private final String contentPlaceholderBegin;
   private final String contentPlaceholderEnd;
   private final String contentPlaceholderFormatSeparator;
   private final String pathPlaceholderBegin;
   private final String pathPlaceholderEnd;
   private final String pathPlaceholderFormatSeparator;
   private final BiDirectionalFilter synchronizeFilter;
   private final BiDirectionalFilter replaceContentFilter;
   private final Map<String, String> properties;
   private final boolean inferPlaceholders;

   public ProjectionConfiguration(Builder builder) {
      this.templateUri = builder.templateUri;
      this.attributes = immutable(builder.attributes.stream().collect(toMap(Attribute::name, identity())));
      this.contentPlaceholderBegin =
            nullable(builder.contentPlaceholderBegin, DEFAULT_CONTENT_PLACEHOLDER_BEGIN);
      this.contentPlaceholderEnd = nullable(builder.contentPlaceholderEnd, DEFAULT_CONTENT_PLACEHOLDER_END);
      this.contentPlaceholderFormatSeparator =
            nullable(builder.contentPlaceholderFormatSeparator, DEFAULT_CONTENT_PLACEHOLDER_FORMAT_SEPARATOR);
      this.pathPlaceholderBegin = nullable(builder.pathPlaceholderBegin, DEFAULT_PATH_PLACEHOLDER_BEGIN);
      this.pathPlaceholderEnd = nullable(builder.pathPlaceholderEnd, DEFAULT_PATH_PLACEHOLDER_END);
      this.pathPlaceholderFormatSeparator =
            nullable(builder.pathPlaceholderFormatSeparator, DEFAULT_PATH_PLACEHOLDER_FORMAT_SEPARATOR);
      this.synchronizeFilter = nullable(builder.synchronizeFilter, new BiDirectionalFilter());
      this.replaceContentFilter = nullable(builder.replaceContentFilter, new BiDirectionalFilter());
      this.properties = nullable(builder.properties, map());
      this.inferPlaceholders = nullable(builder.inferPlaceholders, false);
   }

   @Override
   protected Fields<? extends ProjectionConfiguration> defineDomainFields() {
      return Fields
            .<ProjectionConfiguration>builder()
            .field("templateUri", v -> v.templateUri)
            .field("attributes", v -> v.attributes, MapRules.hasNoNullElements())
            .field("contentPlaceholderBegin", v -> v.contentPlaceholderBegin, isNotBlank())
            .field("contentPlaceholderEnd", v -> v.contentPlaceholderEnd, isNotBlank())
            .field("contentPlaceholderFormatSeparator",
                   v -> v.contentPlaceholderFormatSeparator,
                   isNotBlank())
            .field("pathPlaceholderBegin", v -> v.pathPlaceholderBegin, isNotBlank())
            .field("pathPlaceholderEnd", v -> v.pathPlaceholderEnd, isNotBlank())
            .field("pathPlaceholderFormatSeparator", v -> v.pathPlaceholderFormatSeparator, isNotBlank())
            .field("synchronizeIncludeExclude", v -> v.synchronizeFilter, isNotNull())
            .field("replaceContentIncludeExclude", v -> v.replaceContentFilter, isNotNull())
            .field("properties",
                   v -> v.properties,
                   hasNoNullValues().andValue(keys(allSatisfies(isNotBlank()))))
            .field("inferPlaceholders", v -> v.inferPlaceholders)
            .build();
   }

   public Optional<URI> templateUri() {
      return nullable(templateUri);
   }

   public List<Attribute> attributes() {
      return list(attributes.values());
   }

   public Optional<Attribute> attribute(String name) {
      return nullable(attributes.get(name));
   }

   public String contentPlaceholderBegin() {
      return contentPlaceholderBegin;
   }

   public String contentPlaceholderEnd() {
      return contentPlaceholderEnd;
   }

   public String contentPlaceholderFormatSeparator() {
      return contentPlaceholderFormatSeparator;
   }

   public String pathPlaceholderBegin() {
      return pathPlaceholderBegin;
   }

   public String pathPlaceholderEnd() {
      return pathPlaceholderEnd;
   }

   public String pathPlaceholderFormatSeparator() {
      return pathPlaceholderFormatSeparator;
   }

   public BiDirectionalFilter synchronizeFilter() {
      return synchronizeFilter;
   }

   public BiDirectionalFilter replaceContentFilter() {
      return replaceContentFilter;
   }

   public Map<String, String> properties() {
      return properties;
   }

   public boolean inferPlaceholders() {
      return inferPlaceholders;
   }

   // FIXME this is not a real builder, just a mutable property holder ?!
   protected static class Builder {
      private URI templateUri;
      private List<Attribute> attributes;
      private String contentPlaceholderBegin;
      private String contentPlaceholderEnd;
      private String contentPlaceholderFormatSeparator;
      private String pathPlaceholderBegin;
      private String pathPlaceholderEnd;
      private String pathPlaceholderFormatSeparator;
      private BiDirectionalFilter synchronizeFilter;
      private BiDirectionalFilter replaceContentFilter;
      private Map<String, String> properties;
      private Boolean inferPlaceholders;

      public static Builder from(ProjectionConfiguration projectConfiguration) {
         return new Builder()
               .templateUri(projectConfiguration.templateUri().orElse(null))
               .attributes(projectConfiguration.attributes())
               .contentPlaceholderBegin(projectConfiguration.contentPlaceholderBegin())
               .contentPlaceholderEnd(projectConfiguration.contentPlaceholderEnd())
               .contentPlaceholderFormatSeparator(projectConfiguration.contentPlaceholderFormatSeparator())
               .pathPlaceholderBegin(projectConfiguration.pathPlaceholderBegin())
               .pathPlaceholderEnd(projectConfiguration.pathPlaceholderEnd())
               .pathPlaceholderFormatSeparator(projectConfiguration.pathPlaceholderFormatSeparator())
               .synchronizeFilter(projectConfiguration.synchronizeFilter())
               .replaceContentFilter(projectConfiguration.replaceContentFilter())
               .properties(projectConfiguration.properties())
               .inferPlaceholders(projectConfiguration.inferPlaceholders());
      }

      public Builder templateUri(URI templateUri) {
         this.templateUri = templateUri;
         return this;
      }

      public Builder attributes(List<Attribute> attributes) {
         this.attributes = attributes;
         return this;
      }

      public Builder contentPlaceholderBegin(String contentPlaceholderBegin) {
         this.contentPlaceholderBegin = contentPlaceholderBegin;
         return this;
      }

      public Builder contentPlaceholderEnd(String contentPlaceholderEnd) {
         this.contentPlaceholderEnd = contentPlaceholderEnd;
         return this;
      }

      public Builder contentPlaceholderFormatSeparator(String contentPlaceholderFormatSeparator) {
         this.contentPlaceholderFormatSeparator = contentPlaceholderFormatSeparator;
         return this;
      }

      public Builder pathPlaceholderBegin(String pathPlaceholderBegin) {
         this.pathPlaceholderBegin = pathPlaceholderBegin;
         return this;
      }

      public Builder pathPlaceholderEnd(String pathPlaceholderEnd) {
         this.pathPlaceholderEnd = pathPlaceholderEnd;
         return this;
      }

      public Builder pathPlaceholderFormatSeparator(String pathPlaceholderFormatSeparator) {
         this.pathPlaceholderFormatSeparator = pathPlaceholderFormatSeparator;
         return this;
      }

      public Builder synchronizeFilter(BiDirectionalFilter synchronizeFilter) {
         this.synchronizeFilter = synchronizeFilter;
         return this;
      }

      public Builder replaceContentFilter(BiDirectionalFilter replaceContentFilter) {
         this.replaceContentFilter = replaceContentFilter;
         return this;
      }

      public Builder properties(Map<String, String> properties) {
         this.properties = properties;
         return this;
      }

      public Builder inferPlaceholders(Boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
         return this;
      }
   }

   public static class Filter extends AbstractValue {
      private final List<Path> includes;
      private final List<Path> excludes;

      public Filter(List<Path> includes, List<Path> excludes) {
         this.includes = immutable(list(stream(includes).map(Filter::relativizePath)));
         this.excludes = immutable(list(stream(excludes).map(Filter::relativizePath)));
      }

      public Filter() {
         this(list(), list());
      }

      @Override
      protected Fields<? extends Filter> defineDomainFields() {
         return Fields
               .<Filter>builder()
               .field("includes", v -> v.includes, isValidPathList())
               .field("excludes", v -> v.excludes, isValidPathList())
               .build();
      }

      public List<Path> includes() {
         return includes;
      }

      public List<Path> excludes() {
         return excludes;
      }

      private static InvariantRule<List<Path>> isValidPathList() {
         return CollectionRules
               .<List<Path>, Path>hasNoNullElements()
               .andValue(hasNoDuplicates())
               .andValue(allSatisfies(isNotEmpty().andValue(isNotAbsolute())));
      }

      private static Path relativizePath(Path path) {
         String[] pathParts = StringUtils.split(path.toString(), ":", 2);
         if (pathParts.length > 1 && list("regex", "glob").contains(pathParts[0])) {
            return Path.of(pathParts[0] + ':' + relativizePath(Path.of(pathParts[1])));
         } else {
            return Path.of(StringUtils.stripStart(path.toString(), "/"));
         }
      }

      public boolean isEmpty() {
         return includes.isEmpty() && excludes.isEmpty();
      }
   }

   public static class BiDirectionalFilter extends AbstractValue {
      private final Filter defaultFilter;
      private final Filter downstreamFilter;
      private final Filter upstreamFilter;

      public BiDirectionalFilter(Filter defaultFilter, Filter downstreamFilter, Filter upstreamFilter) {
         this.defaultFilter = defaultFilter;
         this.downstreamFilter = downstreamFilter;
         this.upstreamFilter = upstreamFilter;
      }

      public BiDirectionalFilter(Filter defaultFilter) {
         this(defaultFilter, new Filter(), new Filter());
      }

      public BiDirectionalFilter() {
         this(new Filter(), new Filter(), new Filter());
      }

      @Override
      protected Fields<? extends BiDirectionalFilter> defineDomainFields() {
         return Fields
               .<BiDirectionalFilter>builder()
               .field("defaultFilter", v -> v.defaultFilter, isNotNull())
               .field("downstreamFilter", v -> v.downstreamFilter, isNotNull())
               .field("upstreamFilter", v -> v.upstreamFilter, isNotNull())
               .build();
      }

      public Filter defaultFilter() {
         return defaultFilter;
      }

      public Filter downstreamFilter() {
         return downstreamFilter;
      }

      public Filter upstreamFilter() {
         return upstreamFilter;
      }

      public Optional<DocumentEntrySpecification> downstreamDocumentEntrySpecification(
            DocumentEntrySpecification defaultIncludeSpecification,
            DocumentEntrySpecification defaultExcludeSpecification) {
         return documentEntrySpecification(true,
                                           false,
                                           defaultIncludeSpecification,
                                           defaultExcludeSpecification);
      }

      public Optional<DocumentEntrySpecification> upstreamDocumentEntrySpecification(
            DocumentEntrySpecification defaultIncludeSpecification,
            DocumentEntrySpecification defaultExcludeSpecification) {
         return documentEntrySpecification(false,
                                           true,
                                           defaultIncludeSpecification,
                                           defaultExcludeSpecification);
      }

      private Optional<DocumentEntrySpecification> documentEntrySpecification(boolean downstream,
                                                                              boolean upstream,
                                                                              DocumentEntrySpecification defaultIncludeSpecification,
                                                                              DocumentEntrySpecification defaultExcludeSpecification) {
         List<Path> includes = list(streamConcat(stream(defaultFilter.includes()),
                                                 downstream ? stream(downstreamFilter.includes()) : stream(),
                                                 upstream ? stream(upstreamFilter.includes()) : stream()));

         Optional<DocumentEntrySpecification> includeSpecification =
               includes.isEmpty() && defaultIncludeSpecification == null
               ? optional()
               : optional(DocumentEntrySpecification.orSpecification(list(streamConcat(stream(nullable(
                                                                                             defaultIncludeSpecification)),
                                                                                       stream(includes).map(
                                                                                             BiDirectionalFilter::includeDocumentPathSpecification)))));

         List<Path> excludes = list(streamConcat(stream(defaultFilter.excludes()),
                                                 downstream ? stream(downstreamFilter.excludes()) : stream(),
                                                 upstream ? stream(upstreamFilter.excludes()) : stream()));
         Optional<DocumentEntrySpecification> excludeSpecification =
               excludes.isEmpty() && defaultExcludeSpecification == null
               ? optional()
               : optional(DocumentEntrySpecification.andSpecification(list(streamConcat(stream(nullable(
                                                                                              defaultExcludeSpecification)),
                                                                                        stream(excludes).map(
                                                                                              BiDirectionalFilter::excludeDocumentPathSpecification)))));

         if (includeSpecification.isEmpty() && excludeSpecification.isEmpty()) {
            return optional();
         } else {
            return optional(DocumentEntrySpecification.andSpecification(list(streamConcat(stream(
                  includeSpecification), stream(excludeSpecification)))));
         }
      }

      private static DocumentEntryCriteria includeDocumentPathSpecification(Path path) {
         if (path.toString().startsWith("glob:") || path.toString().startsWith("regex:")) {
            return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.match(path)).build();
         } else {
            return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.equal(path)).build();
         }
      }

      private static DocumentEntryCriteria excludeDocumentPathSpecification(Path path) {
         if (path.toString().startsWith("glob:") || path.toString().startsWith("regex:")) {
            return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.notMatch(path)).build();
         } else {
            return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.notEqual(path)).build();
         }
      }

      public boolean isEmpty() {
         return defaultFilter.isEmpty() && upstreamFilter.isEmpty() && downstreamFilter.isEmpty();
      }
   }
}