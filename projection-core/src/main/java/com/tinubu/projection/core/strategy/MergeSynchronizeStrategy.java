/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.strategy;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.git.ContentMergeStrategy.CONFLICT;
import static com.tinubu.commons.ports.document.git.FastForward.FAST_FORWARD;

import java.io.IOException;
import java.util.Optional;
import java.util.StringJoiner;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevSort;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.git.ContentMergeStrategy;
import com.tinubu.commons.ports.document.git.FastForward;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.GitDocumentConfigBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentRepository;
import com.tinubu.commons.ports.document.git.MergeStatus;
import com.tinubu.commons.ports.document.git.exception.EmptyCommitGitException;
import com.tinubu.commons.ports.document.git.exception.MergeFailedGitException;
import com.tinubu.projection.core.ProjectionProperties;
import com.tinubu.projection.core.ProjectionProperties.Mappers;
import com.tinubu.projection.core.ProjectionProperties.View;

/**
 * Strategy relying on GIT and merge to synchronize changes.
 * <p>
 * The principle is to maintain a "projection"-dedicated branch starting from current "working" branch's
 * root.
 * The synchronization is made on this branch which is then merged into current branch, so that changes can
 * be made on current working branch, after synchronization, and memorized thanks to GIT.
 * <p>
 * It's normal that conflicts occurs in the process. In this case the synchronization fails with some
 * errors, and you have to manually correct the conflicts and finish the merge.
 * <p>
 * The following properties are used :
 * <ul>
 *    <li>{@code projection.strategy.merge.branch} [{@value DEFAULT_PROJECTION_BRANCH}]</li>
 *    <li>{@code projection.strategy.merge.ff} [{@code FAST_FORWARD}]</li>
 * </ul>
 */
public class MergeSynchronizeStrategy implements SynchronizeStrategy {

   private static final String DEFAULT_PROJECTION_BRANCH = "projection";
   private static final FastForward DEFAULT_MERGE_FAST_FORWARD = FAST_FORWARD;
   private static final ContentMergeStrategy DEFAULT_CONTENT_MERGE_STRATEGY = CONFLICT;
   private static final String MERGE_STRATEGY_NAME = "merge";

   private static final Logger log = LoggerFactory.getLogger(MergeSynchronizeStrategy.class);

   private final DocumentRepository templateRepository;
   private final DocumentRepository projectRepository;
   private final SynchronizeStrategy delegateStrategy;
   private final String currentBranch;
   private final GitDocumentRepository gitProjectRepository;
   private final String projectionBranch;
   private final FastForward mergeFastForward;
   private final ContentMergeStrategy contentMergeStrategy;

   public MergeSynchronizeStrategy(boolean overwrite,
                                   DocumentRepository templateRepository,
                                   DocumentRepository projectRepository,
                                   ProjectionProperties properties) {
      validate(projectRepository,
               "projectRepository",
               isNotNull().andValue(isInstanceOf(value(FsDocumentRepository.class),
                                                 "project repository must be a file-system repository when using '%s' strategy",
                                                 MessageValue.value(MERGE_STRATEGY_NAME))))
            .and(validate(templateRepository, "templateRepository", isNotNull()))
            .and(validate(properties, "properties", isNotNull()))
            .orThrow();

      this.templateRepository = templateRepository;
      this.projectRepository = projectRepository;

      this.gitProjectRepository = new GitDocumentRepository((FsDocumentRepository) projectRepository,
                                                            new GitDocumentConfigBuilder()
                                                                  .cloneRepositoryIfMissing(false)
                                                                  .initRepositoryIfMissing(false)
                                                                  .build());
      this.delegateStrategy =
            new DirectSynchronizeStrategy(overwrite, templateRepository, gitProjectRepository, properties);

      View mergeProperties = properties.view("projection.strategy.merge");

      this.projectionBranch = mergeProperties.value("branch").orElse(DEFAULT_PROJECTION_BRANCH);
      this.mergeFastForward =
            mergeProperties.value("ff", Mappers::fastForward).orElse(DEFAULT_MERGE_FAST_FORWARD);
      this.contentMergeStrategy = mergeProperties
            .value("content-merge-strategy", Mappers::contentMergeStrategy)
            .orElse(DEFAULT_CONTENT_MERGE_STRATEGY);
      this.currentBranch = gitProjectRepository.branch();

      if (!gitProjectRepository.isClean()) {
         throw new SynchronizeStrategyException(String.format(
               "GIT '%s' repository is not clean, manually fix the repository first",
               projectRepository));
      }

      RevCommit rootCommit = branchRootCommit();
      String projectionStartRevision = nullable(rootCommit).map(RevCommit::getName).orElse("HEAD");

      log.info(
            "Set 'merge' strategy to synchronize current '{}' branch. Use '{}' as projection branch (startRevision={})",
            currentBranch,
            projectionBranch,
            projectionStartRevision);

      gitProjectRepository.checkout(projectionBranch, true, projectionStartRevision);
   }

   @Override
   public String name() {
      return MERGE_STRATEGY_NAME;
   }

   /**
    * Returns the current branch root commit (first commit without parents). It's theoretically possible to
    * have several root commits, in this case, returns the first one of these.
    *
    * @return current branch root commit, or {@code null} if none found
    */
   private RevCommit branchRootCommit() {
      Git git = gitProjectRepository.git();

      try (RevWalk walk = new RevWalk(git.getRepository())) {
         ObjectId from = git.getRepository().resolve("HEAD");
         if (from == null) {
            return null;
         }
         walk.markStart(walk.parseCommit(from));
         walk.setRetainBody(false);
         walk.sort(RevSort.REVERSE);
         walk.setRevFilter(new RevFilter() {
            @Override
            public boolean include(RevWalk walker, RevCommit c) {
               return c.getParentCount() == 0;
            }

            @Override
            public boolean requiresCommitBody() {
               return false;
            }

            @Override
            public RevFilter clone() {
               return this;
            }

            @Override
            public String toString() {
               return "BRANCH_ROOT";
            }
         });
         return walk.next();
      } catch (IOException e) {
         throw new SynchronizeStrategyException(e);
      }
   }

   @Override
   public Optional<DocumentEntry> synchronizeDocument(Document templateDocument, Document projectDocument) {
      return this.delegateStrategy.synchronizeDocument(templateDocument, projectDocument);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document projectDocument) throws SynchronizeStrategyException {
      return this.delegateStrategy.saveDocument(projectDocument);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document projectDocument, boolean overwrite) {
      return this.delegateStrategy.saveDocument(projectDocument, overwrite);
   }

   @Override
   public void finish() throws SynchronizeStrategyException {
      try {
         merge();
      } finally {
         gitProjectRepository.close();
      }
   }

   private void merge() {
      try {
         gitProjectRepository.addAllDocumentsToIndex();
         try {
            gitProjectRepository.commitIndexedDocuments(synchronizationCommitMessage(), false, false);
         } catch (EmptyCommitGitException ignored) { }
         gitProjectRepository.checkout(currentBranch, false);
         MergeStatus mergeStatus = gitProjectRepository.merge(projectionBranch,
                                                              mergeFastForward,
                                                              contentMergeStrategy,
                                                              "Projection: merge synchronization");
         log.info("Merge successful with '{}' status", mergeStatus);
      } catch (MergeFailedGitException e) {
         throw new SynchronizeStrategyException(String.format(
               "Merge failed with conflicts, please solve conflicts and merge manually > %s",
               e.getMessage()), e);
      }
   }

   private String synchronizationCommitMessage() {
      return String.format("Projection: synchronize '%s' -> '%s'",
                           templateRepository.toUri(),
                           projectRepository.toUri());
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", MergeSynchronizeStrategy.class.getSimpleName() + "[", "]")
            .add("templateRepository=" + templateRepository)
            .add("projectRepository=" + projectRepository)
            .add("delegateStrategy=" + delegateStrategy)
            .toString();
   }

   public static class MergeSynchronizeStrategyFactory implements SynchronizeStrategyFactory {

      @Override
      public String name() {
         return MERGE_STRATEGY_NAME;
      }

      @Override
      public SynchronizeStrategy synchronizeStrategy(boolean overwrite,
                                                     DocumentRepository templateRepository,
                                                     DocumentRepository projectRepository,
                                                     ProjectionProperties properties) {
         return new MergeSynchronizeStrategy(overwrite, templateRepository, projectRepository, properties);
      }
   }

}