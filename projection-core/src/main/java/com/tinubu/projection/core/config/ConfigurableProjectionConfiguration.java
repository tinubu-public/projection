/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.config;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.projection.core.Attribute;

/**
 * Configurable configuration.
 *
 * @implSpec Immutable class implementation
 */
public class ConfigurableProjectionConfiguration extends ProjectionConfiguration {
   private ConfigurableProjectionConfiguration(Builder builder) {
      super(new ProjectionConfiguration.Builder()
                  .templateUri(builder.templateRepository)
                  .attributes(list(builder.attributes.values()))
                  .contentPlaceholderBegin(builder.contentPlaceholderBegin)
                  .contentPlaceholderEnd(builder.contentPlaceholderEnd)
                  .contentPlaceholderFormatSeparator(builder.contentPlaceholderFormatSeparator)
                  .pathPlaceholderBegin(builder.pathPlaceholderBegin)
                  .pathPlaceholderEnd(builder.pathPlaceholderEnd)
                  .pathPlaceholderFormatSeparator(builder.pathPlaceholderFormatSeparator)
                  .synchronizeFilter(builder.synchronizeIncludeExclude)
                  .replaceContentFilter(builder.filterIncludeExclude)
                  .properties(builder.properties)
                  .inferPlaceholders(builder.inferPlaceholders));
   }

   public static ConfigurableProjectionConfiguration empty() {
      return new ConfigurableProjectionConfiguration(new Builder());
   }

   public ConfigurableProjectionConfiguration templateRepository(URI templateRepository) {
      notNull(templateRepository, "templateRepository");

      return new ConfigurableProjectionConfiguration(Builder.from(this).templateRepository(templateRepository));
   }

   public ConfigurableProjectionConfiguration addAttributes(List<Attribute> attributes) {
      return new ConfigurableProjectionConfiguration(Builder.from(this).addAttributes(attributes));
   }

   public ConfigurableProjectionConfiguration addAttributes(Attribute... attributes) {
      return new ConfigurableProjectionConfiguration(Builder.from(this).addAttributes(attributes));
   }

   public ConfigurableProjectionConfiguration addFilterIncludes(List<MimeType> filterIncludes) {
      return new ConfigurableProjectionConfiguration(Builder.from(this).addFilterIncludes(filterIncludes));
   }

   public ConfigurableProjectionConfiguration addFilterIncludes(MimeType... filterIncludes) {
      return new ConfigurableProjectionConfiguration(Builder.from(this).addFilterIncludes(filterIncludes));
   }

   public ConfigurableProjectionConfiguration addProperties(Map<String, String> properties) {
      return new ConfigurableProjectionConfiguration(Builder.from(this).addProperties(properties));
   }

   public ConfigurableProjectionConfiguration addProperty(String name, String value) {
      return new ConfigurableProjectionConfiguration(Builder.from(this).addProperty(name, value));
   }

   public static class Builder extends DomainBuilder<ConfigurableProjectionConfiguration> {
      private URI templateRepository;
      private Map<String, Attribute> attributes = map(LinkedHashMap::new);
      private List<MimeType> filterIncludes = list();
      private String contentPlaceholderBegin;
      private String contentPlaceholderEnd;
      private String contentPlaceholderFormatSeparator;
      private String pathPlaceholderBegin;
      private String pathPlaceholderEnd;
      private String pathPlaceholderFormatSeparator;
      private BiDirectionalFilter synchronizeIncludeExclude;
      private BiDirectionalFilter filterIncludeExclude;
      private Map<String, String> properties = map();
      private Boolean inferPlaceholders;

      public static Builder from(ProjectionConfiguration projectConfiguration) {
         return new Builder()
               .<Builder>reconstitute()
               .templateRepository(projectConfiguration.templateUri().orElse(null))
               .attributes(projectConfiguration.attributes())
               .contentPlaceholderBegin(projectConfiguration.contentPlaceholderBegin())
               .contentPlaceholderEnd(projectConfiguration.contentPlaceholderEnd())
               .contentPlaceholderFormatSeparator(projectConfiguration.contentPlaceholderFormatSeparator())
               .pathPlaceholderBegin(projectConfiguration.pathPlaceholderBegin())
               .pathPlaceholderEnd(projectConfiguration.pathPlaceholderEnd())
               .pathPlaceholderFormatSeparator(projectConfiguration.pathPlaceholderFormatSeparator())
               .synchronizeIncludeExclude(projectConfiguration.synchronizeFilter())
               .filterIncludeExclude(projectConfiguration.replaceContentFilter())
               .properties(projectConfiguration.properties())
               .inferPlaceholders(projectConfiguration.inferPlaceholders());
      }

      public Builder templateRepository(URI templateRepository) {
         this.templateRepository = templateRepository;
         return this;
      }

      public Builder attributes(List<Attribute> attributes) {
         this.attributes = stream(attributes).collect(toMap(Attribute::name, identity()));
         return this;
      }

      public Builder addAttributes(List<Attribute> attributes) {
         this.attributes.putAll(stream(attributes).collect(toMap(Attribute::name, identity())));
         return this;
      }

      public Builder addAttributes(Attribute... attributes) {
         return addAttributes(list(attributes));
      }

      public Builder filterIncludes(List<MimeType> filterIncludes) {
         this.filterIncludes = list(filterIncludes);
         return this;
      }

      public Builder addFilterIncludes(List<MimeType> filterIncludes) {
         this.filterIncludes.addAll(list(filterIncludes));
         return this;
      }

      public Builder addFilterIncludes(MimeType... filterIncludes) {
         this.filterIncludes.addAll(list(filterIncludes));
         return this;
      }

      public Builder contentPlaceholderBegin(String contentPlaceholderBegin) {
         this.contentPlaceholderBegin = contentPlaceholderBegin;
         return this;
      }

      public Builder contentPlaceholderEnd(String contentPlaceholderEnd) {
         this.contentPlaceholderEnd = contentPlaceholderEnd;
         return this;
      }

      public Builder contentPlaceholderFormatSeparator(String contentPlaceholderFormatSeparator) {
         this.contentPlaceholderFormatSeparator = contentPlaceholderFormatSeparator;
         return this;
      }

      public Builder pathPlaceholderBegin(String pathPlaceholderBegin) {
         this.pathPlaceholderBegin = pathPlaceholderBegin;
         return this;
      }

      public Builder pathPlaceholderEnd(String pathPlaceholderEnd) {
         this.pathPlaceholderEnd = pathPlaceholderEnd;
         return this;
      }

      public Builder pathPlaceholderFormatSeparator(String pathPlaceholderFormatSeparator) {
         this.pathPlaceholderFormatSeparator = pathPlaceholderFormatSeparator;
         return this;
      }

      public Builder synchronizeIncludeExclude(BiDirectionalFilter synchronizeIncludeExclude) {
         this.synchronizeIncludeExclude = synchronizeIncludeExclude;
         return this;
      }

      public Builder filterIncludeExclude(BiDirectionalFilter filterIncludeExclude) {
         this.filterIncludeExclude = filterIncludeExclude;
         return this;
      }

      public Builder properties(Map<String, String> properties) {
         this.properties = map(properties);
         return this;
      }

      public Builder addProperties(Map<String, String> properties) {
         this.properties.putAll(map(properties));
         return this;
      }

      public Builder addProperty(String name, String value) {
         this.properties.put(name, value);
         return this;
      }

      public Builder inferPlaceholders(Boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
         return this;
      }

      @Override
      protected ConfigurableProjectionConfiguration buildDomainObject() {
         return new ConfigurableProjectionConfiguration(this);
      }
   }

}
