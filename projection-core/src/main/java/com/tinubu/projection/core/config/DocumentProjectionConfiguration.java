/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.config;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_YAML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toMap;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.nodes.NodeTuple;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.projection.core.Attribute;

public class DocumentProjectionConfiguration extends ProjectionConfiguration {
   private static final Yaml YAML_PARSER = yamlParser();
   private static final Yaml YAML_DUMPER = yamlDumper();

   private DocumentProjectionConfiguration(Builder builder) {
      super(builder);
   }

   public static DocumentProjectionConfiguration of(ProjectionConfiguration projectionConfiguration) {
      return new DocumentProjectionConfiguration(Builder.from(projectionConfiguration));
   }

   public static DocumentProjectionConfiguration ofDocument(Document projectionConfiguration) {
      try (InputStream content = projectionConfiguration.content().inputStreamContent()) {
         ProjectionConfigurationModel projectionConfigurationModel;

         try {
            projectionConfigurationModel = projectionConfigurationModel(content).getProjection();
         } catch (Exception e) {
            throw new IllegalStateException(String.format("Can't parse '%s' projection configuration > %s",
                                                          projectionConfiguration.documentId().stringValue(),
                                                          e.getMessage()), e);
         }

         return new DocumentProjectionConfiguration(new Builder()
                                                          .templateUri(nullable(projectionConfigurationModel.getTemplateUri())
                                                                             .map(URI::create)
                                                                             .orElse(null))
                                                          .attributes(projectionConfigurationModel.attributes())
                                                          .contentPlaceholderBegin(
                                                                projectionConfigurationModel.getContentPlaceholderBegin())
                                                          .contentPlaceholderEnd(projectionConfigurationModel.getContentPlaceholderEnd())
                                                          .contentPlaceholderFormatSeparator(
                                                                projectionConfigurationModel.getContentPlaceholderFormatSeparator())
                                                          .pathPlaceholderBegin(projectionConfigurationModel.getPathPlaceholderBegin())
                                                          .pathPlaceholderEnd(projectionConfigurationModel.getPathPlaceholderEnd())
                                                          .pathPlaceholderFormatSeparator(
                                                                projectionConfigurationModel.getPathPlaceholderFormatSeparator())
                                                          .synchronizeFilter(projectionConfigurationModel
                                                                                   .getSynchronize()
                                                                                   .biDirectionalFilter())
                                                          .replaceContentFilter(projectionConfigurationModel
                                                                                      .getReplaceContent()
                                                                                      .biDirectionalFilter())
                                                          .properties(projectionConfigurationModel.getProperties())
                                                          .inferPlaceholders(projectionConfigurationModel.getInferPlaceholders()));
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private static RootProjectionConfigurationModel projectionConfigurationModel(InputStream content) {
      return nullable(YAML_PARSER.loadAs(content, RootProjectionConfigurationModel.class),
                      new RootProjectionConfigurationModel());
   }

   public Document toDocument(DocumentPath documentId) {
      RootProjectionConfigurationModel rootProjectionConfiguration =
            new RootProjectionConfigurationModel(this);

      return new DocumentBuilder()
            .documentId(documentId)
            .contentType(APPLICATION_YAML)
            .loadedContent(YAML_DUMPER.dump(rootProjectionConfiguration), UTF_8)
            .build();
   }

   private static Yaml yamlParser() {
      return new Yaml();
   }

   private static Yaml yamlDumper() {
      DumperOptions options = new DumperOptions();

      options.setCanonical(false);
      options.setAllowUnicode(true);
      options.setPrettyFlow(false);
      options.setDefaultFlowStyle(FlowStyle.BLOCK);
      options.setIndent(3);

      Representer representer = new HideEmptyRepresenter(options);
      representer.setDefaultFlowStyle(FlowStyle.BLOCK);
      representer.addClassTag(RootProjectionConfigurationModel.class, Tag.MAP);

      return new Yaml(representer, options);
   }

   private static class HideEmptyRepresenter extends Representer {
      public HideEmptyRepresenter(DumperOptions options) {
         super(options);
      }

      @Override
      protected NodeTuple representJavaBeanProperty(Object javaBean,
                                                    Property property,
                                                    Object propertyValue,
                                                    Tag customTag) {
         if (propertyValue instanceof List && ((List<?>) propertyValue).isEmpty()) {
            return null;
         } else if (propertyValue instanceof Map && ((Map<?, ?>) propertyValue).isEmpty()) {
            return null;
         } else if (propertyValue instanceof RootFilterConfigurationModel
                    && ((RootFilterConfigurationModel) propertyValue).isEmpty()) {
            return null;
         } else if (propertyValue instanceof FilterConfigurationModel
                    && ((FilterConfigurationModel) propertyValue).isEmpty()) {
            return null;
         } else {
            return super.representJavaBeanProperty(javaBean, property, propertyValue, customTag);
         }
      }
   }

   public static class RootProjectionConfigurationModel {
      private ProjectionConfigurationModel projection;

      public ProjectionConfigurationModel getProjection() {
         return projection;
      }

      public RootProjectionConfigurationModel() {
         this.projection = new ProjectionConfigurationModel();
      }

      public RootProjectionConfigurationModel(ProjectionConfiguration projectionConfiguration) {
         this.projection = new ProjectionConfigurationModel();
         this.projection.setTemplateUri(projectionConfiguration
                                              .templateUri()
                                              .map(URI::toString)
                                              .orElse(null));
         this.projection.setAttributes(projectionConfiguration
                                             .attributes()
                                             .stream()
                                             .collect(toMap(Attribute::decoratedName, Attribute::value)));
         this.projection.setContentPlaceholderBegin(projectionConfiguration.contentPlaceholderBegin());
         this.projection.setContentPlaceholderEnd(projectionConfiguration.contentPlaceholderEnd());
         this.projection.setContentPlaceholderFormatSeparator(projectionConfiguration.contentPlaceholderFormatSeparator());
         this.projection.setPathPlaceholderBegin(projectionConfiguration.pathPlaceholderBegin());
         this.projection.setPathPlaceholderEnd(projectionConfiguration.pathPlaceholderEnd());
         this.projection.setPathPlaceholderFormatSeparator(projectionConfiguration.pathPlaceholderFormatSeparator());
         RootFilterConfigurationModel synchronize =
               new RootFilterConfigurationModel(projectionConfiguration.synchronizeFilter());
         this.projection.setSynchronize(synchronize);
         RootFilterConfigurationModel replaceContent =
               new RootFilterConfigurationModel(projectionConfiguration.replaceContentFilter());
         this.projection.setReplaceContent(replaceContent);
         this.projection.setProperties(projectionConfiguration.properties());
         this.projection.setInferPlaceholders(projectionConfiguration.inferPlaceholders());
      }

      public void setProjection(ProjectionConfigurationModel projection) {
         this.projection = nullable(projection, new ProjectionConfigurationModel());
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this).append("projection", projection).toString();
      }
   }

   public static class ProjectionConfigurationModel {
      private String templateUri;
      private Map<String, String> attributes = map();
      private String contentPlaceholderBegin;
      private String contentPlaceholderEnd;
      private String contentPlaceholderFormatSeparator;
      private String pathPlaceholderBegin;
      private String pathPlaceholderEnd;
      private String pathPlaceholderFormatSeparator;
      private RootFilterConfigurationModel synchronize = new RootFilterConfigurationModel();
      private RootFilterConfigurationModel replaceContent = new RootFilterConfigurationModel();
      private Map<String, String> properties = map();
      private Boolean inferPlaceholders;

      public String getTemplateUri() {
         return templateUri;
      }

      public void setTemplateUri(String templateUri) {
         this.templateUri = templateUri;
      }

      public Map<String, String> getAttributes() {
         return attributes;
      }

      public void setAttributes(Map<String, String> attributes) {
         this.attributes = map(attributes);
      }

      public List<Attribute> attributes() {
         return list(attributes.entrySet().stream().map(e -> Attribute.of(e.getKey(), e.getValue())));
      }

      public String getContentPlaceholderBegin() {
         return contentPlaceholderBegin;
      }

      public void setContentPlaceholderBegin(String contentPlaceholderBegin) {
         this.contentPlaceholderBegin = contentPlaceholderBegin;
      }

      public String getContentPlaceholderEnd() {
         return contentPlaceholderEnd;
      }

      public void setContentPlaceholderEnd(String contentPlaceholderEnd) {
         this.contentPlaceholderEnd = contentPlaceholderEnd;
      }

      public String getContentPlaceholderFormatSeparator() {
         return contentPlaceholderFormatSeparator;
      }

      public void setContentPlaceholderFormatSeparator(String contentPlaceholderFormatSeparator) {
         this.contentPlaceholderFormatSeparator = contentPlaceholderFormatSeparator;
      }

      public String getPathPlaceholderBegin() {
         return pathPlaceholderBegin;
      }

      public void setPathPlaceholderBegin(String pathPlaceholderBegin) {
         this.pathPlaceholderBegin = pathPlaceholderBegin;
      }

      public String getPathPlaceholderEnd() {
         return pathPlaceholderEnd;
      }

      public void setPathPlaceholderEnd(String pathPlaceholderEnd) {
         this.pathPlaceholderEnd = pathPlaceholderEnd;
      }

      public String getPathPlaceholderFormatSeparator() {
         return pathPlaceholderFormatSeparator;
      }

      public void setPathPlaceholderFormatSeparator(String pathPlaceholderFormatSeparator) {
         this.pathPlaceholderFormatSeparator = pathPlaceholderFormatSeparator;
      }

      public RootFilterConfigurationModel getSynchronize() {
         return synchronize;
      }

      public void setSynchronize(RootFilterConfigurationModel synchronize) {
         this.synchronize = nullable(synchronize, new RootFilterConfigurationModel());
      }

      public RootFilterConfigurationModel getReplaceContent() {
         return replaceContent;
      }

      public void setReplaceContent(RootFilterConfigurationModel replaceContent) {
         this.replaceContent = nullable(replaceContent, new RootFilterConfigurationModel());
      }

      public Map<String, String> getProperties() {
         return properties;
      }

      public void setProperties(Map<String, String> properties) {
         this.properties = map(properties);
      }

      public Boolean getInferPlaceholders() {
         return inferPlaceholders;
      }

      public void setInferPlaceholders(Boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", ProjectionConfigurationModel.class.getSimpleName() + "[", "]")
               .add("templateUri=" + templateUri)
               .add("attributes=" + attributes)
               .add("contentPlaceholderBegin='" + contentPlaceholderBegin + "'")
               .add("contentPlaceholderEnd='" + contentPlaceholderEnd + "'")
               .add("contentPlaceholderFormatSeparator='" + contentPlaceholderFormatSeparator + "'")
               .add("pathPlaceholderBegin='" + pathPlaceholderBegin + "'")
               .add("pathPlaceholderEnd='" + pathPlaceholderEnd + "'")
               .add("pathPlaceholderFormatSeparator='" + pathPlaceholderFormatSeparator + "'")
               .add("synchronize='" + synchronize + "'")
               .add("replaceContent='" + replaceContent + "'")
               .add("properties='" + properties + "'")
               .add("inferPlaceholders='" + inferPlaceholders + "'")
               .toString();
      }
   }

   public static class FilterConfigurationModel {
      private List<String> includes = list();
      private List<String> excludes = list();

      public FilterConfigurationModel() {
      }

      public FilterConfigurationModel(Filter filter) {
         this.includes = list(filter.includes().stream().map(Path::toString));
         this.excludes = list(filter.excludes().stream().map(Path::toString));

      }

      public List<String> getIncludes() {
         return includes;
      }

      public void setIncludes(List<String> includes) {
         this.includes = list(includes);
      }

      public List<String> getExcludes() {
         return excludes;
      }

      public void setExcludes(List<String> excludes) {
         this.excludes = list(excludes);
      }

      public Filter filter() {
         return new Filter(list(stream(includes).map(Path::of)), list(stream(excludes).map(Path::of)));
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", FilterConfigurationModel.class.getSimpleName() + "[", "]")
               .add("includes=" + includes)
               .add("excludes=" + excludes)
               .toString();
      }

      public boolean isEmpty() {
         return includes.isEmpty() && excludes.isEmpty();
      }
   }

   public static class RootFilterConfigurationModel {
      private List<String> includes = list();
      private List<String> excludes = list();
      private FilterConfigurationModel downstream = new FilterConfigurationModel();
      private FilterConfigurationModel upstream = new FilterConfigurationModel();

      public RootFilterConfigurationModel() {
      }

      public RootFilterConfigurationModel(BiDirectionalFilter biDirectionalFilter) {
         this.includes = list(biDirectionalFilter.defaultFilter().includes().stream().map(Path::toString));
         this.excludes = list(biDirectionalFilter.defaultFilter().excludes().stream().map(Path::toString));
         this.downstream = new FilterConfigurationModel(biDirectionalFilter.downstreamFilter());
         this.upstream = new FilterConfigurationModel(biDirectionalFilter.upstreamFilter());

      }

      public List<String> getIncludes() {
         return includes;
      }

      public void setIncludes(List<String> includes) {
         this.includes = list(includes);
      }

      public List<String> getExcludes() {
         return excludes;
      }

      public void setExcludes(List<String> excludes) {
         this.excludes = list(excludes);
      }

      public FilterConfigurationModel getDownstream() {
         return downstream;
      }

      public void setDownstream(FilterConfigurationModel downstream) {
         this.downstream = nullable(downstream, new FilterConfigurationModel());
      }

      public FilterConfigurationModel getUpstream() {
         return upstream;
      }

      public void setUpstream(FilterConfigurationModel upstream) {
         this.upstream = nullable(upstream, new FilterConfigurationModel());
      }

      public BiDirectionalFilter biDirectionalFilter() {
         return new BiDirectionalFilter(new Filter(list(stream(includes).map(Path::of)),
                                                   list(stream(excludes).map(Path::of))),
                                        downstream.filter(),
                                        upstream.filter());
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", RootFilterConfigurationModel.class.getSimpleName() + "[", "]")
               .add("includes=" + includes)
               .add("excludes=" + excludes)
               .add("downstream=" + downstream)
               .add("upstream=" + upstream)
               .toString();
      }

      public boolean isEmpty() {
         return includes.isEmpty() && excludes.isEmpty() && downstream.isEmpty() && upstream.isEmpty();
      }
   }

}
