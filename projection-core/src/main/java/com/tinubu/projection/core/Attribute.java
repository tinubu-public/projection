/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.MatchingRules.matches;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.length;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.projection.core.format.Format;

/**
 * Template attribute with an arbitrary name and value. These attributes are used in content and path value
 * replacements, using attribute value inference or explicit placeholders.
 */
public class Attribute extends AbstractValue {
   /**
    * Maximum attribute value size in bytes.
    */
   public static final int ATTRIBUTE_MAX_SIZE = 32768;
   /**
    * Suffix notation for attribute names to notify an attribute is required.
    */
   private static final String REQUIRED_SUFFIX = "*";
   private static final Pattern NAME_REGEX =
         Pattern.compile("[A-Za-z-_](?:" + Pattern.quote(REQUIRED_SUFFIX) + ")?");

   private final String name;
   private final String value;
   private final boolean required;

   private Attribute(String name, String value) {
      this.required = name.endsWith(REQUIRED_SUFFIX);
      this.name = StringUtils.removeEnd(name, REQUIRED_SUFFIX);
      this.value = value;
   }

   public static Attribute of(String name, String value) {
      return new Attribute(name, value);
   }

   @Override
   protected Fields<? extends Attribute> defineDomainFields() {
      return Fields
            .<Attribute>builder()
            .field("name",
                   v -> v.name,
                   isNotBlank().andValue(matches(ParameterValue.value(NAME_REGEX, "NAME_REGEX"))))
            .field("value",
                   v -> v.value,
                   isNotBlank().andValue(length(isLessThanOrEqualTo(ParameterValue.value(ATTRIBUTE_MAX_SIZE,
                                                                                         "ATTRIBUTE_MAX_SIZE")))))
            .field("required", v -> v.required)
            .build();
   }

   public String name() {
      return name;
   }

   public String decoratedName() {
      return name + (required ? REQUIRED_SUFFIX : "");
   }

   public String value() {
      return value;
   }

   public boolean required() {
      return required;
   }

   /**
    * Returns groups of ambiguous formats. Each group of ambiguous formats results is associated to the same
    * and unique formatted attribute value.
    *
    * @param formats formats to check
    *
    * @return groups of ambiguous formats
    */
   public List<List<Format>> ambiguousFormats(List<Format> formats) {
      LinkedHashMap<Format, String> results = new LinkedHashMap<>();
      for (Format format : formats) {
         results.put(format, format.format(value, false));
      }

      return list(results
                        .entrySet()
                        .stream()
                        .collect(groupingBy(Entry::getValue,
                                            LinkedHashMap::new,
                                            mapping(Entry::getKey, toList())))
                        .values());
   }
}
