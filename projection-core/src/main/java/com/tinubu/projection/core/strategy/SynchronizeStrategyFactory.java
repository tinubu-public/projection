/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.strategy;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.projection.core.ProjectionProperties;

@FunctionalInterface
public interface SynchronizeStrategyFactory {

   /**
    * Creates new strategy instance.
    *
    * @param overwrite overwrite mode
    * @param templateRepository template repository
    * @param projectRepository project repository
    * @param properties properties for this synchronization
    *
    * @return new synchronization strategy
    */
   SynchronizeStrategy synchronizeStrategy(boolean overwrite,
                                           DocumentRepository templateRepository,
                                           DocumentRepository projectRepository,
                                           ProjectionProperties properties);

   /**
    * Name of this strategy.
    *
    * @return strategy name
    */
   default String name() {
      return StringUtils.removeEnd(this.getClass().getSimpleName().toLowerCase(), "factory");
   }

   default boolean supports(Class<? extends SynchronizeStrategy> synchronizeStrategyClass) {
      validate(synchronizeStrategyClass, "synchronizedStrategyClass", isNotNull()).orThrow();

      return synchronizeStrategyClass.isAssignableFrom(getClass());
   }

   default boolean supports(String name) {
      validate(name, "name", isNotBlank()).orThrow();

      return nullable(name()).map(n -> equalsIgnoreCase(n, name)).orElse(false);
   }

}
