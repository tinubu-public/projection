/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.strategy;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.util.ServiceLoader;

import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.projection.core.ProjectionProperties;

/**
 * Special strategy factory supporting {link ServiceLoader}.
 * <p>
 * You can provide your own strategies using the
 * {@code com.tinubu.projection.core.strategy.SynchronizeStrategyFactory} service loader.
 */
public class ServiceLoaderSynchronizeStrategyFactory implements SynchronizeStrategyFactory {

   private final SynchronizeStrategyFactory synchronizeStrategyFactory;

   public ServiceLoaderSynchronizeStrategyFactory(Class<? extends SynchronizeStrategy> synchronizeStrategyClass) {
      validate(synchronizeStrategyClass, "synchronizeStrategyClass", isNotNull()).orThrow();

      this.synchronizeStrategyFactory = stream(ServiceLoader.load(SynchronizeStrategyFactory.class))
            .filter(factory -> factory.supports(synchronizeStrategyClass))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(String.format(
                  "No factory found for '%s' synchronize strategy",
                  synchronizeStrategyClass.getName())));
   }

   public ServiceLoaderSynchronizeStrategyFactory(String synchronizeStrategyName) {
      validate(synchronizeStrategyName, "synchronizeStrategyName", isNotBlank()).orThrow();

      this.synchronizeStrategyFactory = stream(ServiceLoader.load(SynchronizeStrategyFactory.class))
            .filter(factory -> factory.supports(synchronizeStrategyName))
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(String.format(
                  "No factory found for '%s' synchronize strategy",
                  synchronizeStrategyName)));
   }

   @Override
   public String name() {
      return synchronizeStrategyFactory.name();
   }

   @Override
   public SynchronizeStrategy synchronizeStrategy(boolean overwrite,
                                                  DocumentRepository templateRepository,
                                                  DocumentRepository projectRepository,
                                                  ProjectionProperties properties) {
      return synchronizeStrategyFactory.synchronizeStrategy(overwrite,
                                                            templateRepository,
                                                            projectRepository,
                                                            properties);
   }
}
