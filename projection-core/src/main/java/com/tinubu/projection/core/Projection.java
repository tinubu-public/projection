/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import static com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder;
import static com.tinubu.projection.core.LoggerUtils.lazy;
import static com.tinubu.projection.core.ProjectionProperties.Mappers;
import static java.util.stream.Collectors.joining;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ports.document.classpath.ClasspathDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.DocumentRepositoryFactory;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Authentication;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.GitDocumentConfigBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Transport.TransportBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentRepository;
import com.tinubu.commons.ports.document.sftp.SftpDocumentRepository;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategy;
import com.tinubu.projection.core.ProjectionProperties.View;
import com.tinubu.projection.core.config.ConfigurableProjectionConfiguration;
import com.tinubu.projection.core.config.DocumentProjectionConfiguration;
import com.tinubu.projection.core.config.ProjectionConfiguration;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.FormatFactory;
import com.tinubu.projection.core.strategy.DirectSynchronizeStrategy.DirectSynchronizeStrategyFactory;
import com.tinubu.projection.core.strategy.SynchronizeStrategy;
import com.tinubu.projection.core.strategy.SynchronizeStrategyFactory;

/**
 * Projection main class.
 */
public class Projection {

   /**
    * Default exclude specification for template repository. These defaults are always used except
    * configuration exclusions are set.
    */
   private static final DocumentEntryCriteria TEMPLATE_DEFAULT_EXCLUDE =
         new DocumentEntryCriteriaBuilder().build();

   /**
    * GIT data exclude specification for template repository.
    */
   private static final DocumentEntryCriteria TEMPLATE_GIT_EXCLUDE = new DocumentEntryCriteriaBuilder()
         .documentPath(CriterionBuilder.notMatch(Path.of("glob:.git/**")))
         .build();

   /** Used formats. */
   private static final List<Format> FORMATS = new FormatFactory().formats();

   private static final Logger log = LoggerFactory.getLogger(Projection.class);
   private static final ProjectionRepository projectionRepository = new ProjectionRepository();

   private DocumentRepository projectRepository;
   private URI projectUri;
   private boolean internalProjectRepository = false;
   private boolean overwrite = false;
   private ProjectionProperties properties = new ProjectionProperties();

   public Projection properties(Map<String, String> properties) {
      this.properties = new ProjectionProperties();
      this.properties.registerProperties(map(properties), true);
      return this;
   }

   public ProjectionSynchronize synchronize() {
      return new ProjectionSynchronize();
   }

   public ProjectionUpdate update() {
      return new ProjectionUpdate();
   }

   /** Synchronize sub-command. */
   public class ProjectionSynchronize implements AutoCloseable {

      private DocumentRepository templateRepository;
      private URI templateUri;
      private boolean internalTemplateRepository = false;
      private ProjectionConfiguration templateConfiguration;
      private ProjectionConfiguration projectConfiguration;
      private List<Attribute> templateAttributes = list();
      private List<Attribute> projectAttributes = list();
      private boolean inferPlaceholders = false;
      private boolean excludeGit = true;
      private boolean generateProjectionConfiguration = true;
      private boolean generateProjectionMetadata = true;
      private SynchronizeStrategyFactory synchronizeStrategyFactory = new DirectSynchronizeStrategyFactory();

      public ProjectionSynchronize templateRepository(DocumentRepository templateRepository) {
         validate(this.templateUri, isNull("template URI is already set")).orThrow();

         this.templateRepository = validate(templateRepository, "templateRepository", isNotNull()).orThrow();

         return this;
      }

      public ProjectionSynchronize templateRepository(URI templateUri) {
         validate(this.templateRepository, isNull("template repository is already set")).orThrow();

         this.templateUri = validate(templateUri, "templateUri", isNotNull()).orThrow();
         this.internalTemplateRepository = true;

         return this;
      }

      public ProjectionSynchronize templateRepository(String templateUri) {
         return templateRepository(URI.create(validate(templateUri, "templateUri", isNotBlank()).orThrow()));
      }

      public ProjectionSynchronize projectRepository(DocumentRepository projectRepository) {
         validate(Projection.this.projectUri, isNull("project URI is already set")).orThrow();

         Projection.this.projectRepository =
               validate(projectRepository, "projectRepository", isNotNull()).orThrow();

         return this;
      }

      public ProjectionSynchronize projectRepository(URI projectUri) {
         validate(Projection.this.projectRepository, isNull("project repository is already set")).orThrow();

         Projection.this.projectUri = validate(projectUri, "projectUri", isNotNull()).orThrow();
         Projection.this.internalProjectRepository = true;

         return this;
      }

      public ProjectionSynchronize projectRepository(String projectUri) {
         return projectRepository(URI.create(validate(projectUri, "projectUri", isNotBlank()).orThrow()));
      }

      public ProjectionSynchronize templateConfiguration(ProjectionConfiguration templateConfiguration) {
         this.templateConfiguration = templateConfiguration;
         return this;
      }

      public ProjectionSynchronize projectConfiguration(ProjectionConfiguration projectConfiguration) {
         this.projectConfiguration = projectConfiguration;
         return this;
      }

      public ProjectionSynchronize templateAttributes(List<Attribute> templateAttributes) {
         this.templateAttributes = templateAttributes;
         return this;
      }

      public ProjectionSynchronize templateAttributes(Attribute... templateAttributes) {
         return templateAttributes(list(templateAttributes));
      }

      public ProjectionSynchronize projectAttributes(List<Attribute> projectAttributes) {
         this.projectAttributes = projectAttributes;
         return this;
      }

      public ProjectionSynchronize projectAttributes(Attribute... projectAttributes) {
         return projectAttributes(list(projectAttributes));
      }

      public ProjectionSynchronize overwrite(boolean overwrite) {
         Projection.this.overwrite = overwrite;
         return this;
      }

      public ProjectionSynchronize inferPlaceholders(boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
         return this;
      }

      public ProjectionSynchronize excludeGit(boolean excludeGit) {
         this.excludeGit = excludeGit;
         return this;
      }

      public ProjectionSynchronize generateProjectionConfiguration(boolean generateProjectionConfiguration) {
         this.generateProjectionConfiguration = generateProjectionConfiguration;
         return this;
      }

      public ProjectionSynchronize generateProjectionMetadata(boolean generateProjectionMetadata) {
         this.generateProjectionMetadata = generateProjectionMetadata;
         return this;
      }

      public ProjectionSynchronize synchronizeStrategyFactory(SynchronizeStrategyFactory synchronizeStrategyFactory) {
         this.synchronizeStrategyFactory = synchronizeStrategyFactory;
         return this;
      }

      /**
       * Synchronizes all documents from template repository to project repository, only considering project
       * primary projection. Sub-projections are excluded from synchronization if existing.
       */
      public void synchronize() {
         projectRepository = projectRepository(Projection.this.properties).orElse(null);

         validate(projectRepository, "projectRepository", isNotNull()).orThrow();

         projectConfiguration = nullable(projectConfiguration,
                                         () -> projectionConfiguration(projectRepository,
                                                                       projectAttributes,
                                                                       properties.keepProperties(),
                                                                       inferPlaceholders,
                                                                       () -> new ConfigurableProjectionConfiguration.Builder()
                                                                             .templateRepository(
                                                                                   templateRepositoryUri().orElse(
                                                                                         null))
                                                                             .build()));

         ProjectionProperties templateProperties = new ProjectionProperties(properties);
         templateProperties.registerProperties(projectConfiguration.properties(), false);

         templateRepository = templateRepository(templateProperties).orElseGet(() -> {
            internalTemplateRepository = true;
            return templateRepositoryFromConfiguration(projectConfiguration, templateProperties);
         });

         verifyRepository(templateRepository, projectRepository);

         templateConfiguration = nullable(templateConfiguration,
                                          projectionConfiguration(templateRepository,
                                                                  templateAttributes,
                                                                  properties.keepProperties(),
                                                                  null,
                                                                  ConfigurableProjectionConfiguration::empty));

         verifyConfiguration(templateConfiguration, projectConfiguration);

         ProjectionProperties properties = new ProjectionProperties(projectConfiguration.properties());

         SynchronizeStrategy synchronizeStrategy =
               synchronizeStrategy(templateRepository, Projection.this.projectRepository, properties);

         Projection.this
               .synchronize(templateRepository,
                            projectRepository,
                            templateConfiguration,
                            projectConfiguration,
                            synchronizeStrategy,
                            documentSynchronizeProcessor(templateConfiguration, projectConfiguration),
                            excludeGit)
               .forEach(__ -> { /* iterate stream */ });

         if (generateProjectionConfiguration) {
            Projection.this.generateProjectionConfiguration(projectConfiguration, synchronizeStrategy);
         }
         if (generateProjectionMetadata) {
            Projection.this.generateProjectionMetadata(templateRepository,
                                                       projectConfiguration,
                                                       synchronizeStrategy);
         }

         synchronizeStrategy.finish();
      }

      private Optional<DocumentRepository> templateRepository(ProjectionProperties properties) {
         return nullable(templateRepository).or(() -> nullable(templateUri).map(uri -> repository(uri,
                                                                                                  false,
                                                                                                  properties)));
      }

      private Optional<DocumentRepository> projectRepository(ProjectionProperties properties) {
         return nullable(projectRepository).or(() -> nullable(projectUri).map(uri -> repository(uri,
                                                                                                true,
                                                                                                properties)));
      }

      private Optional<URI> templateRepositoryUri() {
         return nullable(templateRepository)
               .filter(templateRepository -> templateRepository.hasCapability(REPOSITORY_URI))
               .map(UriAdapter::toUri)
               .or(() -> nullable(templateUri));
      }

      private SynchronizeStrategy synchronizeStrategy(DocumentRepository templateRepository,
                                                      DocumentRepository projectRepository,
                                                      ProjectionProperties properties) {
         return synchronizeStrategyFactory.synchronizeStrategy(overwrite,
                                                               templateRepository,
                                                               projectRepository,
                                                               properties);
      }

      @Override
      public void close() {
         try {
            if (templateRepository != null && internalTemplateRepository) {
               templateRepository.close();
            }
         } finally {
            if (projectRepository != null && internalProjectRepository) {
               projectRepository.close();
            }
         }
      }

   }

   /** Update template sub-command. */
   public class ProjectionUpdate implements AutoCloseable {

      private DocumentRepository templateRepository;
      private URI templateUri;
      private boolean internalTemplateRepository = false;
      private ProjectionConfiguration templateConfiguration;
      private ProjectionConfiguration projectConfiguration;
      private List<Attribute> templateAttributes = list();
      private List<Attribute> projectAttributes = list();
      private boolean inferPlaceholders = false;

      public ProjectionUpdate templateRepository(DocumentRepository templateRepository) {
         validate(this.templateUri, isNull("template URI is already set")).orThrow();

         this.templateRepository = validate(templateRepository, "templateRepository", isNotNull()).orThrow();

         return this;
      }

      public ProjectionUpdate templateRepository(URI templateUri) {
         validate(this.templateRepository, isNull("template repository is already set")).orThrow();

         this.templateUri = validate(templateUri, "templateUri", isNotNull()).orThrow();
         this.internalTemplateRepository = true;

         return this;
      }

      public ProjectionUpdate templateRepository(String templateUri) {
         return templateRepository(URI.create(validate(templateUri, "templateUri", isNotBlank()).orThrow()));
      }

      public ProjectionUpdate projectRepository(DocumentRepository projectRepository) {
         validate(Projection.this.projectUri, isNull("project URI is already set")).orThrow();

         Projection.this.projectRepository =
               validate(projectRepository, "projectRepository", isNotNull()).orThrow();

         return this;
      }

      public ProjectionUpdate projectRepository(URI projectUri) {
         validate(Projection.this.projectRepository, isNull("project repository is already set")).orThrow();

         Projection.this.projectUri = validate(projectUri, "projectUri", isNotNull()).orThrow();
         Projection.this.internalProjectRepository = true;

         return this;
      }

      public ProjectionUpdate projectRepository(String projectUri) {
         return projectRepository(URI.create(validate(projectUri, "projectUri", isNotBlank()).orThrow()));
      }

      public ProjectionUpdate templateConfiguration(ProjectionConfiguration templateConfiguration) {
         this.templateConfiguration = templateConfiguration;
         return this;
      }

      public ProjectionUpdate projectConfiguration(ProjectionConfiguration projectConfiguration) {
         this.projectConfiguration = projectConfiguration;
         return this;
      }

      public ProjectionUpdate templateAttributes(List<Attribute> templateAttributes) {
         this.templateAttributes = templateAttributes;
         return this;
      }

      public ProjectionUpdate templateAttributes(Attribute... templateAttributes) {
         return templateAttributes(list(templateAttributes));
      }

      public ProjectionUpdate projectAttributes(List<Attribute> projectAttributes) {
         this.projectAttributes = projectAttributes;
         return this;
      }

      public ProjectionUpdate projectAttributes(Attribute... projectAttributes) {
         return projectAttributes(list(projectAttributes));
      }

      public ProjectionUpdate overwrite(boolean overwrite) {
         Projection.this.overwrite = overwrite;
         return this;
      }

      public ProjectionUpdate inferPlaceholders(boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
         return this;
      }

      public void update(DocumentPath... projectDocuments) {
         update(list(projectDocuments));
      }

      public void update(List<DocumentPath> projectDocuments) {
         update(new DocumentEntryCriteriaBuilder()
                      .documentPath(CriterionBuilder.<Path>in(list(stream(projectDocuments).map(DocumentPath::value))))
                      .build());
      }

      public void update(DocumentEntryCriteria projectDocumentSpecification) {
         validate(projectDocumentSpecification, "projectDocumentSpecification", isNotNull()).orThrow();

         projectRepository = projectRepository(Projection.this.properties).orElse(null);

         validate(projectRepository, "projectRepository", isNotNull()).orThrow();

         projectConfiguration = nullable(projectConfiguration,
                                         () -> projectionConfiguration(projectRepository,
                                                                       projectAttributes,
                                                                       properties.keepProperties(),
                                                                       null,
                                                                       () -> new ConfigurableProjectionConfiguration.Builder()
                                                                             .templateRepository(
                                                                                   templateRepositoryUri().orElse(
                                                                                         null))
                                                                             .build()));

         ProjectionProperties templateProperties = new ProjectionProperties(properties);
         templateProperties.registerProperties(projectConfiguration.properties(), false);

         templateRepository = templateRepository(templateProperties).orElseGet(() -> {
            internalTemplateRepository = true;
            return templateRepositoryFromConfiguration(projectConfiguration, templateProperties);
         });

         verifyRepository(templateRepository, projectRepository);

         templateConfiguration = nullable(templateConfiguration,
                                          projectionConfiguration(templateRepository,
                                                                  templateAttributes,
                                                                  properties.keepProperties(),
                                                                  inferPlaceholders,
                                                                  ConfigurableProjectionConfiguration::empty));

         verifyConfiguration(templateConfiguration, projectConfiguration);

         DocumentSynchronizeProcessor synchronizeProcessor =
               new DocumentSynchronizeProcessor(projectConfiguration, templateConfiguration, FORMATS);

         stream(projectRepository.findDocumentsBySpecification(projectDocumentSpecification))
               .flatMap(projectDocument -> {
                  Document templateDocument = synchronizeProcessor.process(projectDocument);

                  return stream(update(projectDocument, templateDocument));
               })
               .forEach(__ -> { /* iterate stream */ });
      }

      private Optional<DocumentEntry> update(Document projectDocument, Document templateDocument) {
         return peek(templateRepository.saveDocument(templateDocument, overwrite),
                     savedDocument -> log.info("Updated '{}' -> '{}'",
                                               lazy(() -> projectRepository.documentDisplayId(projectDocument.documentId())),
                                               lazy(() -> projectRepository.documentDisplayId(templateDocument.documentId())))).or(
               () -> {
                  log.warn("Can't overwrite '{}' -> '{}'",
                           lazy(() -> projectRepository.documentDisplayId(projectDocument.documentId())),
                           lazy(() -> projectRepository.documentDisplayId(templateDocument.documentId())));
                  return optional();
               });
      }

      private Optional<DocumentRepository> templateRepository(ProjectionProperties properties) {
         return nullable(templateRepository).or(() -> nullable(templateUri).map(uri -> repository(uri,
                                                                                                  false,
                                                                                                  properties)));
      }

      private Optional<DocumentRepository> projectRepository(ProjectionProperties properties) {
         return nullable(projectRepository).or(() -> nullable(projectUri).map(uri -> repository(uri,
                                                                                                true,
                                                                                                properties)));
      }

      private Optional<URI> templateRepositoryUri() {
         if (!templateRepository.hasCapability(REPOSITORY_URI)) {
            return optional();
         }
         try {
            return nullable(templateRepository).map(DocumentRepository::toUri);
         } catch (UnsupportedCapabilityException e) {
            return optional();
         }
      }

      @Override
      public void close() {
         try {
            if (templateRepository != null && internalTemplateRepository) {
               templateRepository.close();
            }
         } finally {
            if (projectRepository != null && internalProjectRepository) {
               projectRepository.close();
            }
         }
      }

   }

   private DocumentSynchronizeProcessor documentSynchronizeProcessor(ProjectionConfiguration templateConfiguration,
                                                                     ProjectionConfiguration projectConfiguration) {
      return new DocumentSynchronizeProcessor(templateConfiguration, projectConfiguration, FORMATS);
   }

   /**
    * Effective documents synchronization and content filtering.
    *
    * @param templateRepository template source repository
    * @param projectRepository project target repository
    * @param templateConfiguration template configuration
    * @param projectConfiguration project configuration
    * @param synchronizeStrategy synchronization strategy
    * @param synchronizeProcessor content filtering processor
    *
    * @return synchronized document entries
    */
   private Stream<DocumentEntry> synchronize(DocumentRepository templateRepository,
                                             DocumentRepository projectRepository,
                                             ProjectionConfiguration templateConfiguration,
                                             ProjectionConfiguration projectConfiguration,
                                             SynchronizeStrategy synchronizeStrategy,
                                             DocumentSynchronizeProcessor synchronizeProcessor,
                                             boolean ignoreGit) {
      notNull(templateRepository, "templateRepository");
      notNull(projectRepository, "projectRepository");
      notNull(templateConfiguration, "templateConfiguration");
      notNull(projectConfiguration, "projectConfiguration");
      notNull(synchronizeStrategy, "synchronizeStrategy");
      notNull(synchronizeProcessor, "synchronizeProcessor");

      log.info("Synchronize '{}' to '{}' using '{}' strategy",
               lazy(templateRepository::toUri),
               lazy(projectRepository::toUri),
               synchronizeStrategy.name());

      DocumentEntrySpecification projectionDocumentsSpecification =
            filterProjectDocuments(projectRepository, projectConfiguration, true);

      return templateRepository
            .findDocumentsBySpecification(filterTemplateDocuments(templateConfiguration, ignoreGit))
            .flatMap(templateDocument -> {
               Document projectDocument = synchronizeProcessor.process(templateDocument);

               if (projectionDocumentsSpecification.satisfiedBy(projectDocument.documentEntry())) {
                  return stream(synchronizeStrategy.synchronizeDocument(templateDocument, projectDocument));
               } else {
                  return stream();
               }
            });
   }

   /**
    * Returns template repository instantiated from URI configured in project configuration.
    *
    * @param projectConfiguration project configuration
    * @param properties
    *
    * @return new template document repository
    */
   private DocumentRepository templateRepositoryFromConfiguration(ProjectionConfiguration projectConfiguration,
                                                                  ProjectionProperties properties) {
      return projectConfiguration
            .templateUri()
            .map(uri -> repository(uri, false, properties))
            .orElseThrow(() -> new IllegalStateException("Missing template URI in project configuration"));
   }

   /**
    * Generates metadata in projection application path. Metadata are stored as different
    * documents :
    * <ul>
    *    <li>{@code LAST_SYNCHRONIZATION_DATE}</li>
    *    <li>{@code LAST_SYNCHRONIZATION_CONFIGURATION}</li>
    *    <li>{@code LAST_SYNCHRONIZATION_COMMIT}</li>
    * </ul>
    *  @param templateRepository template repository
    *
    * @param projectConfiguration project configuration
    * @param synchronizeStrategy synchronize strategy
    */
   private void generateProjectionMetadata(DocumentRepository templateRepository,
                                           ProjectionConfiguration projectConfiguration,
                                           SynchronizeStrategy synchronizeStrategy) {
      Document lastSynchronizationDate = new DocumentBuilder()
            .documentId(DocumentPath.of(projectionRepository
                                              .projectionDirectory()
                                              .resolve("LAST_SYNCHRONIZATION_DATE")))
            .loadedContent(DateTimeFormatter.ISO_INSTANT.format(Instant.now()), StandardCharsets.UTF_8)
            .build();

      synchronizeStrategy.saveDocument(lastSynchronizationDate, true);

      Document lastSynchronizationConfiguration = DocumentProjectionConfiguration
            .of(projectConfiguration)
            .toDocument(DocumentPath.of(projectionRepository
                                              .projectionDirectory()
                                              .resolve("LAST_SYNCHRONIZATION_CONFIGURATION")));

      synchronizeStrategy.saveDocument(lastSynchronizationConfiguration, true);

      if (templateRepository instanceof GitDocumentRepository) {
         ((GitDocumentRepository) templateRepository).resolveObjectId("HEAD").ifPresent(lastCommit -> {
            Document lastSynchronizationCommit = new DocumentBuilder()
                  .documentId(DocumentPath.of(projectionRepository
                                                    .projectionDirectory()
                                                    .resolve("LAST_SYNCHRONIZATION_COMMIT")))
                  .loadedContent(lastCommit, StandardCharsets.UTF_8)
                  .build();

            synchronizeStrategy.saveDocument(lastSynchronizationCommit, true);
         });
      }
   }

   /**
    * Generates the new project configuration using specified synchronization strategy.
    *
    * @param projectConfiguration project configuration
    * @param synchronizeStrategy synchronize strategy
    *
    * @return created document entry or {@link Optional#empty} if document not created
    */
   private Optional<DocumentEntry> generateProjectionConfiguration(ProjectionConfiguration projectConfiguration,
                                                                   SynchronizeStrategy synchronizeStrategy) {
      notNull(projectConfiguration, "projectConfiguration");
      notNull(synchronizeStrategy, "synchronizeStrategy");

      DocumentPath configurationPath = DocumentPath.of(projectionRepository.projectionConfiguration());

      return synchronizeStrategy.saveDocument(DocumentProjectionConfiguration
                                                    .of(projectConfiguration)
                                                    .toDocument(configurationPath));
   }

   /**
    * Verifies repositories consistency.
    *
    * @param templateRepository template repository
    * @param projectRepository project repository
    */
   private static void verifyRepository(DocumentRepository templateRepository,
                                        DocumentRepository projectRepository) {
      notNull(templateRepository, "templateRepository");
      notNull(projectRepository, "projectRepository");

      if (projectRepository.sameRepositoryAs(templateRepository)) {
         throw new IllegalStateException("Can't synchronize when template and project URIs are the same");
      }
   }

   /**
    * Verifies configurations completeness and consistency.
    *
    * @param templateConfiguration template configuration
    * @param projectConfiguration project configuration
    */
   private static void verifyConfiguration(ProjectionConfiguration templateConfiguration,
                                           ProjectionConfiguration projectConfiguration) {
      notNull(templateConfiguration, "templateConfiguration");
      notNull(projectConfiguration, "projectConfiguration");

      log.debug("Template configuration = {}", templateConfiguration);
      log.debug("Project configuration = {}", projectConfiguration);

      if (!projectConfiguration.inferPlaceholders()) {
         for (Attribute templateAttribute : templateConfiguration.attributes()) {
            Attribute projectAttribute = projectConfiguration.attribute(templateAttribute.name()).or(() -> {
                                                                if (templateAttribute.required()) {
                                                                   return optional();
                                                                } else {
                                                                   log.info("Missing optional '{}' attribute", templateAttribute.name());
                                                                   return optional(templateAttribute);
                                                                }
                  })
                  .orElseThrow(() -> new IllegalStateException(String.format("Missing project '%s' attribute",
                                                                             templateAttribute.name())));

            if (!org.apache.commons.collections4.CollectionUtils.isEqualCollection(projectAttribute.ambiguousFormats(
                  FORMATS), templateAttribute.ambiguousFormats(FORMATS))) {
               log.warn("Template attribute '{}' : '{}' -> '{}' has ambiguous format inference",
                        templateAttribute.name(),
                        templateAttribute.value(),
                        projectAttribute.value());
            }
         }
      }
   }

   /**
    * Generates document specification filter from template point of view.
    *
    * @param templateConfiguration template configuration
    *
    * @return document specification
    */
   private static DocumentEntrySpecification filterTemplateDocuments(ProjectionConfiguration templateConfiguration,
                                                                     boolean ignoreGit) {
      DocumentEntrySpecification templateFilter = templateConfiguration
            .synchronizeFilter()
            .upstreamDocumentEntrySpecification(null,
                                                TEMPLATE_DEFAULT_EXCLUDE.and(ignoreGit
                                                                             ? TEMPLATE_GIT_EXCLUDE
                                                                             : DocumentEntrySpecification.allDocuments()))
            .orElseGet(DocumentEntryCriteriaBuilder::allDocuments);

      return new DocumentEntryCriteriaBuilder()
            .documentPath(CriterionBuilder.notMatch(projectionRepository.projectionConfigurationGlobMatcher(
                  projectionRepository.projectionConfiguration(),
                  true,
                  true)))
            .build()
            .and(templateFilter);
   }

   /**
    * Generates document specification filter from project point of view.
    *
    * @param projectRepository project repository
    * @param projectConfiguration project configuration
    * @param excludeSubProjections if sub-projections must be filtered out
    *
    * @return document specification
    */
   private static DocumentEntrySpecification filterProjectDocuments(DocumentRepository projectRepository,
                                                                    ProjectionConfiguration projectConfiguration,
                                                                    boolean excludeSubProjections) {
      DocumentEntrySpecification projectDocumentsSpecification = projectConfiguration
            .synchronizeFilter()
            .downstreamDocumentEntrySpecification(null, null)
            .orElseGet(DocumentEntryCriteriaBuilder::allDocuments);

      if (excludeSubProjections) {
         List<Path> subProjections = list(projectionRepository.subProjections(projectRepository,
                                                                              projectionRepository.projectionConfiguration(),
                                                                              false,
                                                                              true,
                                                                              true));

         if (!subProjections.isEmpty()) {
            log.info("Exclude {} project sub-projections from synchronization",
                     lazy(() -> subProjections.stream().map(Path::toString).collect(joining(",", "[", "]"))));

            DocumentEntrySpecification excludeProjectionSubRootsFilter =
                  DocumentEntrySpecification.andSpecification(list(subProjections
                                                                         .stream()
                                                                         .map(subProjection -> new DocumentEntryCriteriaBuilder()
                                                                               .documentPath(CriterionBuilder.notStartWith(
                                                                                     subProjection))
                                                                               .build())));

            projectDocumentsSpecification =
                  projectDocumentsSpecification.and(excludeProjectionSubRootsFilter);
         }
      }

      return projectDocumentsSpecification;
   }

   /**
    * Instantiates a projection configuration from default configuration in specified repository.
    *
    * @param repository repository to search for default configuration
    * @param attributes optional list of attributes overriding default configuration
    * @param properties optional map of properties overriding default configuration
    * @param inferPlaceholders optional flag to force infer placeholders value in configuration. This
    *       will override current configuration only if not {@code null}
    * @param newConfiguration new configuration builder used if configuration not found
    *
    * @return projection configuration
    */
   private ProjectionConfiguration projectionConfiguration(DocumentRepository repository,
                                                           List<Attribute> attributes,
                                                           Map<String, String> properties,
                                                           Boolean inferPlaceholders,
                                                           Supplier<ProjectionConfiguration> newConfiguration) {
      ProjectionConfiguration configuration = repository
            .findDocumentById(DocumentPath.of(projectionRepository.projectionConfiguration()))
            .<ProjectionConfiguration>map(DocumentProjectionConfiguration::ofDocument)
            .orElseGet(newConfiguration);

      return ConfigurableProjectionConfiguration.Builder
            .from(configuration)
            .<ConfigurableProjectionConfiguration.Builder>conditionalChain(__ -> Boolean.TRUE.equals(
                  inferPlaceholders), b -> b.inferPlaceholders(true))
            .addAttributes(attributes)
            .addProperties(properties)
            .build();
   }

   /**
    * Creates repository from URI.
    *
    * @param properties properties to use for settings
    * @param uri URI, can be absolute or relative.
    *
    * @return repository
    *
    * @implSpec Relative URIs are normalized to absolute {@code file:} URIs
    */
   private DocumentRepository repository(URI uri, boolean projectUri, ProjectionProperties properties) {
      URI absoluteUri = uri;
      if (!uri.isAbsolute()) {
         absoluteUri = Path.of(uri.getPath()).toAbsolutePath().toUri();
      }

      return new DocumentRepositoryFactory()
            .documentRepository(absoluteUri, uriAdapters(absoluteUri, projectUri, properties))
            .orElseThrow(() -> new IllegalStateException(String.format("Unsupported '%s' repository", uri)));
   }

   /**
    * @implNote GIT repository should be listed after Fs/Sftp because its URI can clash with them.
    *       You can use special git:<GIT URI> to force the GIT repository usage in case of ambiguous URI (use
    *       of {@code file:} URI, ...)
    */
   // FIXME ServiceLoader ?
   private List<UriAdapter> uriAdapters(URI uri, boolean projectUri, ProjectionProperties properties) {
      List<UriAdapter> adapters = list();

      sftpDocumentRepository(uri, properties).ifPresent(adapters::add);
      fsDocumentRepository(uri, projectUri).ifPresent(adapters::add);
      if (!Objects.equals(uri.getScheme(), "file")) {
         gitDocumentRepository(uri, projectUri, properties).ifPresent(adapters::add);
      }
      if (!projectUri) {
         classpathDocumentRepository(uri).ifPresent(adapters::add);
      }

      return adapters;
   }

   private Optional<DocumentRepository> gitDocumentRepository(URI uri,
                                                              boolean projectUri,
                                                              ProjectionProperties properties) {
      ProjectionProperties.View git = properties.view("projection.repository.git");

      return new GitDocumentConfigBuilder()
            .fromUri(uri)
            .map(config -> config
                  .pullStrategy(new PullStrategyBuilder()
                                      .pullMode(PullMode.ON_CREATE)
                                      .fastForward(git.nullableValue("pull.ff", Mappers::fastForward))
                                      .rebase(git.nullableValue("pull.rebase", Mappers::booleanValue))
                                      .optionalChain(gitShallowDepth(git), PullStrategyBuilder::shallowDepth)
                                      .build())
                  .cloneRepositoryIfMissing(true)
                  .remote(git.nullableValue("remote"))
                  .branch(git.nullableValue("branch"))
                  .transport(new TransportBuilder()
                                   .allowSshHosts(git.nullableValue("transport.allow-ssh-hosts",
                                                                    Mappers::booleanValue))
                                   .disableSslVerify(git.nullableValue("transport.disable-ssl-verify",
                                                                       Mappers::booleanValue))
                                   .connectTimeout(git.nullableValue("transport.connect-timeout",
                                                                     Mappers::duration))
                                   .build())
                  .<GitDocumentConfigBuilder, String>optionalChain(git.value("authentication.username"),
                                                                   (b, username) -> b.authentication(
                                                                         Authentication.of(username,
                                                                                           git.nullableValue(
                                                                                                 "authentication.password"))))
                  .<GitDocumentConfigBuilder>conditionalChain(__ -> projectUri,
                                                              b -> b.pushStrategy(PushStrategyBuilder.onClose())))
            .map(GitDocumentConfigBuilder::build)
            .map(config -> new GitDocumentRepository(new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build()), config));
   }

   private static Optional<Integer> gitShallowDepth(View git) {
      try {
         return git.value("pull.shallow-depth", Mappers::integer).or(() -> optional(1));
      } catch (InvalidPropertyException e) {
         if (git.requiredValue("pull.shallow-depth", Mappers::booleanValue)) {
            return optional(1);
         } else {
            return optional();
         }
      }
   }

   private Optional<ClasspathDocumentRepository> classpathDocumentRepository(URI uri) {
      return ClasspathDocumentRepository.fromUri(uri);
   }

   private Optional<FsDocumentRepository> fsDocumentRepository(URI uri, boolean projectUri) {
      return new FsDocumentConfigBuilder()
            .fromUri(uri)
            .map(config -> config.createStoragePathIfMissing(projectUri))
            .map(FsDocumentConfigBuilder::build)
            .map(FsDocumentRepository::new);
   }

   private Optional<DocumentRepository> sftpDocumentRepository(URI uri, ProjectionProperties properties) {
      ProjectionProperties.View sftp = properties.view("projection.repository.sftp");

      return new SftpDocumentConfigBuilder()
            .fromUri(uri)
            .map(config -> config
                  .username(sftp.nullableValue("username"))
                  .password(sftp.nullableValue("username"))
                  .keepAliveInterval(sftp.nullableValue("keep-alive-interval", Mappers::duration))
                  .sessionPoolSize(1)
                  .writeStrategyClass(sftp.nullableValue("write-strategy-class",
                                                         value -> Mappers.classObject(value,
                                                                                      WriteStrategy.class)))
                  .privateKey(sftp.nullableValue("private-key", Mappers::base64))
                  .privateKeyPassphrase(sftp.nullableValue("private-key-passphrase")))
            .map(SftpDocumentConfigBuilder::build)
            .map(SftpDocumentRepository::new);
   }

}