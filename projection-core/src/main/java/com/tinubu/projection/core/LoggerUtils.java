/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.util.concurrent.Callable;

public final class LoggerUtils {

   private LoggerUtils() {
   }

   /**
    * Lazy parameter evaluation for SLF4J logging.
    *
    * @param callable lazy parameter evaluation
    *
    * @return parameter value lazily evaluated when {@link Object#toString} will be called
    */
   public static Object lazy(Callable<?> callable) {
      return new Object() {
         @Override
         public String toString() {
            try {
               return nullable(callable.call()).map(Object::toString).orElse(null);
            } catch (Exception e) {
               throw new IllegalStateException(e);
            }
         }
      };
   }
}
