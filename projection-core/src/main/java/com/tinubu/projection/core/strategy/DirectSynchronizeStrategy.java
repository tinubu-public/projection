/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.strategy;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.projection.core.LoggerUtils.lazy;

import java.util.Optional;
import java.util.StringJoiner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.projection.core.ProjectionProperties;

/**
 * Simple direct synchronization strategy. Documents are simply written to project repository using the
 * specified "overwrite" mode.
 */
public class DirectSynchronizeStrategy implements SynchronizeStrategy {

   private static final String STRATEGY_NAME = "direct";

   private static final Logger log = LoggerFactory.getLogger(DirectSynchronizeStrategy.class);

   private final boolean overwrite;
   private final DocumentRepository templateRepository;
   private final DocumentRepository projectRepository;

   public DirectSynchronizeStrategy(boolean overwrite,
                                    DocumentRepository templateRepository,
                                    DocumentRepository projectRepository,
                                    ProjectionProperties properties) {
      validate(templateRepository, "templateRepository", isNotNull())
            .and(validate(projectRepository, "projectRepository", isNotNull()))
            .and(validate(properties, "properties", isNotNull()))
            .orThrow();

      this.overwrite = overwrite;
      this.templateRepository = templateRepository;
      this.projectRepository = projectRepository;
   }

   @Override
   public String name() {
      return STRATEGY_NAME;
   }

   @Override
   public Optional<DocumentEntry> synchronizeDocument(Document templateDocument, Document projectDocument) {
      return peek(projectRepository.saveDocument(projectDocument, overwrite),
                  savedDocument -> log.info("Synchronized '{}' -> '{}'",
                                            lazy(() -> projectRepository.documentDisplayId(templateDocument.documentId())),
                                            lazy(() -> projectRepository.documentDisplayId(projectDocument.documentId())))).or(
            () -> {
               log.warn("Can't overwrite '{}' -> '{}'",
                        lazy(() -> projectRepository.documentDisplayId(templateDocument.documentId())),
                        lazy(() -> projectRepository.documentDisplayId(projectDocument.documentId())));
               return optional();
            });
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document projectDocument) throws SynchronizeStrategyException {
      return saveDocument(projectDocument, overwrite);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document projectDocument, boolean overwrite) {
      return peek(projectRepository.saveDocument(projectDocument, overwrite),
                  savedDocument -> log.info("Saved '{}'",
                                            lazy(() -> projectRepository.documentDisplayId(projectDocument.documentId())))).or(
            () -> {
               log.warn("Can't overwrite '{}'",
                        lazy(() -> projectRepository.documentDisplayId(projectDocument.documentId())));
               return optional();
            });
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DirectSynchronizeStrategy.class.getSimpleName() + "[", "]")
            .add("overwrite=" + overwrite)
            .add("templateRepository=" + templateRepository)
            .add("projectRepository=" + projectRepository)
            .toString();
   }

   public static class DirectSynchronizeStrategyFactory implements SynchronizeStrategyFactory {

      @Override
      public String name() {
         return STRATEGY_NAME;
      }

      @Override
      public SynchronizeStrategy synchronizeStrategy(boolean overwrite,
                                                     DocumentRepository templateRepository,
                                                     DocumentRepository projectRepository,
                                                     ProjectionProperties properties) {
         return new DirectSynchronizeStrategy(overwrite, templateRepository, projectRepository, properties);
      }
   }
}
