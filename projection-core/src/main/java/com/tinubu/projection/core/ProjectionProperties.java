/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullValues;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.keys;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.util.stream.Collectors.toMap;

import java.time.Duration;
import java.time.format.DateTimeParseException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.git.ContentMergeStrategy;
import com.tinubu.commons.ports.document.git.FastForward;

// FIXME introduce an operation to list all properties from different features, including ServiceLoader extensions ?
public class ProjectionProperties {
   /**
    * Suffix to append to property name to mark the property as "sensitive". These properties won't be saved
    * in generated configurations, nor printed in logs.
    */
   private static final String SENSITIVE_PROPERTY_SUFFIX = "!";

   private final Map<String, Pair<String, Boolean>> properties;

   public ProjectionProperties() {
      this.properties = map();
   }

   public ProjectionProperties(Map<String, String> properties) {
      this();
      registerProperties(properties, true);
   }

   public ProjectionProperties(ProjectionProperties properties) {
      this.properties = new HashMap<>(properties.properties);
   }

   public View view() {
      return new View();
   }

   public View view(String prefix) {
      return new View(prefix);
   }

   public void registerProperty(String name, String value, boolean override) {
      validate(name, "name", isNotBlank()).and(validate(name, "value", isNotNull())).orThrow();

      boolean keepProperty = !name.endsWith(SENSITIVE_PROPERTY_SUFFIX);
      if (!keepProperty) {
         name = name.substring(0, name.length() - 1);
      }
      properties.merge(name, Pair.of(value, keepProperty), (v1, v2) -> override ? v2 : v1);
   }

   public void registerProperties(Map<String, String> properties, boolean override) {
      validate(properties,
               "properties",
               hasNoNullValues().andValue(keys(allSatisfies(isNotBlank())))).orThrow();

      properties.forEach((name, value) -> registerProperty(name, value, override));
   }

   public void unregisterAll() {
      properties.clear();
   }

   public Map<String, String> keepProperties() {
      return properties
            .entrySet()
            .stream()
            .filter(e -> e.getValue().getRight())
            .collect(toMap(Entry::getKey, e -> e.getValue().getLeft()));
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ProjectionProperties.class.getSimpleName() + "[", "]")
            .add("properties=" + properties
                  .entrySet()
                  .stream()
                  .collect(toMap(Entry::getKey,
                                 e -> e.getValue().getRight() ? e.getValue().getLeft() : "<sensitive>")))
            .toString();
   }

   public class View {
      private final String prefix;

      private View() {
         this.prefix = null;
      }

      private View(String prefix) {
         this.prefix = validate(prefix, "prefix", isNotBlank()).orThrow();
      }

      public Optional<String> value(String name) {
         validate(name, "name", isNotBlank()).orThrow();

         return nullable(properties.get(prefixedName(name))).map(Pair::getLeft);
      }

      public <T> Optional<T> value(String name, Function<String, ? extends T> mapper) {
         validate(mapper, "mapper", isNotNull()).orThrow();

         return value(name).map(mapper);
      }

      public String requiredValue(String name) {
         return value(name).orElseThrow(() -> new IllegalStateException(String.format("Required '%s' property",
                                                                                      prefixedName(name))));
      }

      public <T> T requiredValue(String name, Function<String, ? extends T> mapper) {
         validate(mapper, "mapper", isNotNull()).orThrow();

         return mapper.apply(requiredValue(name));

      }

      public String nullableValue(String name) {
         return value(name).orElse(null);
      }

      public <T> T nullableValue(String name, Function<String, ? extends T> mapper) {
         validate(mapper, "mapper", isNotNull()).orThrow();

         return value(name).map(mapper).orElse(null);
      }

      private String prefixedName(String name) {
         return prefix != null ? prefix + "." + name : name;
      }
   }

   /**
    * Mappers collection to convert property strings.
    */
   public static class Mappers {

      public static Duration duration(String value) {
         if (value == null) {
            return null;
         }

         try {
            return Duration.parse(value);
         } catch (DateTimeParseException e) {
            throw new InvalidPropertyException(String.format("Can't parse '%s' duration", value), e);
         }
      }

      public static Class<?> classObject(String value) {
         if (value == null) {
            return null;
         }

         try {
            return Class.forName(value);
         } catch (ClassNotFoundException e) {
            throw new InvalidPropertyException(String.format("Unknown '%s' class", value), e);
         }
      }

      @SuppressWarnings("unchecked")
      public static <T> Class<T> classObject(String value, Class<T> clazz) {
         if (value == null) {
            return null;
         }

         try {
            Class<?> classObject = Class.forName(value);

            if (clazz.isAssignableFrom(classObject)) {
               return (Class<T>) classObject;
            } else {
               throw new InvalidPropertyException(String.format("'%s' value is not an instance of '%s'",
                                                                value,
                                                                clazz.getName()));
            }
         } catch (ClassNotFoundException e) {
            throw new InvalidPropertyException(String.format("Unknown '%s' class", value), e);
         }
      }

      public static byte[] base64(String value) {
         if (value == null) {
            return null;
         }

         try {
            return Base64.getDecoder().decode(value);
         } catch (IllegalArgumentException e) {
            throw new InvalidPropertyException(String.format("Can't decode '%s' from Base64", value), e);
         }
      }

      public static Boolean booleanValue(String value) {
         if (value == null) {
            return null;
         }

         try {
            return Boolean.parseBoolean(value);
         } catch (NumberFormatException e) {
            throw new InvalidPropertyException(String.format("Can't parse '%s' boolean", value), e);
         }
      }

      public static Integer integer(String value) {
         if (value == null) {
            return null;
         }

         try {
            return Integer.parseInt(value);
         } catch (NumberFormatException e) {
            throw new InvalidPropertyException(String.format("Can't parse '%s' integer", value), e);
         }
      }

      public static FastForward fastForward(String value) {
         if (value == null) {
            return null;
         }

         return switch (value.toLowerCase()) {
            case "ff", "fastforward", "fast-forward" -> FastForward.FAST_FORWARD;
            case "noff", "no-ff", "no-fast-forward" -> FastForward.NO_FAST_FORWARD;
            case "ffonly", "ff-only", "fast-forward-only" -> FastForward.FAST_FORWARD_ONLY;
            default ->
                  throw new InvalidPropertyException(String.format("Unknown '%s' fast-forward mode", value));
         };
      }

      public static ContentMergeStrategy contentMergeStrategy(String value) {
         if (value == null) {
            return null;
         }

         return switch (value.toLowerCase()) {
            case "conflict" -> ContentMergeStrategy.CONFLICT;
            case "ours", "our" -> ContentMergeStrategy.OURS;
            case "theirs", "their" -> ContentMergeStrategy.THEIRS;
            default -> throw new InvalidPropertyException(String.format("Unknown '%s' content merge strategy",
                                                                        value));
         };
      }
   }
}
