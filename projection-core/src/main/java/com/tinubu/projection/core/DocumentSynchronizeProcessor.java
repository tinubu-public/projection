/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_DOCBOOK;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_JAVASCRIPT;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_JSON;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OCTET_STREAM;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_YAML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import static com.tinubu.projection.core.LoggerUtils.lazy;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.io.GeneralReplacerReader;
import com.tinubu.commons.lang.io.GeneralReplacerReader.TokenReplacer;
import com.tinubu.commons.lang.io.MultiReplacer;
import com.tinubu.commons.lang.io.PlaceholderReplacerReader;
import com.tinubu.commons.lang.io.StringReplacer;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.projection.core.config.ProjectionConfiguration;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.NoopFormat;

/**
 * Document synchronization processor that generates a filtered, and renamed, project document.
 */
public class DocumentSynchronizeProcessor implements DocumentProcessor {
   /**
    * Working buffer size, in bytes, for replacers. Attributes size must not overpass this
    * value.
    * We assume here that all formats will always generate formatted value with a size lesser than or equal
    * to 2 times the source value.
    */
   private static final int REPLACER_BUFFER_SIZE = 2 * Attribute.ATTRIBUTE_MAX_SIZE;

   private static final NoopFormat NOOP_FORMAT = new NoopFormat();
   /**
    * Default content types that are supported for content replacement. These defaults are always used except
    * if explicitly excluded from configuration.
    */
   private static final List<MimeType> DEFAULT_REPLACE_CONTENT_TYPES = list(mimeType("text", "*"),
                                                                            APPLICATION_JSON,
                                                                            APPLICATION_XML,
                                                                            APPLICATION_XHTML,
                                                                            APPLICATION_DOCBOOK,
                                                                            APPLICATION_JAVASCRIPT,
                                                                            APPLICATION_YAML);

   private static final Logger log = LoggerFactory.getLogger(DocumentSynchronizeProcessor.class);

   private final ProjectionConfiguration templateConfiguration;
   private final ProjectionConfiguration projectConfiguration;
   private final Map<String, Format> formats;

   private static final Set<String> missingPlaceholderAttributes = new HashSet<>();

   public DocumentSynchronizeProcessor(ProjectionConfiguration templateConfiguration,
                                       ProjectionConfiguration projectConfiguration,
                                       List<Format> formats) {
      this.templateConfiguration = templateConfiguration;
      this.projectConfiguration = projectConfiguration;
      this.formats = map(LinkedHashMap::new, stream(formats).map(f -> Pair.of(f.name(), f)));
   }

   @Override
   public Document process(Document templateDocument) {
      DocumentEntry projectDocumentEntry = DocumentEntryBuilder
            .from(templateDocument.documentEntry())
            .documentId(filterDocumentId(templateDocument.documentId()))
            .build();

      DocumentBuilder projectDocumentBuilder = new DocumentBuilder()
            .documentId(projectDocumentEntry.documentId())
            .contentType(projectDocumentEntry.metadata().contentType().orElse(APPLICATION_OCTET_STREAM))
            .attributes(projectDocumentEntry.metadata().attributes())
            .content(templateDocument.content());

      if (canReplaceContent(templateDocument.documentEntry(), projectDocumentEntry)) {
         log.debug("[processor] Filter '{}' template document",
                   lazy(() -> templateDocument.documentId().stringValue()));

         Charset contentEncoding =
               templateDocument.metadata().contentEncoding().orElse(Charset.defaultCharset());

         Reader replacedContent = templateDocument.content().readerContent(contentEncoding);
         replacedContent = replaceContentInferredAttributes(replacedContent, false);
         if (!projectConfiguration.inferPlaceholders()) {
            replacedContent = replaceContentPlaceholders(templateDocument.documentId(),
                                                         replacedContent,
                                                         templateConfiguration.contentPlaceholderBegin(),
                                                         templateConfiguration.contentPlaceholderEnd(),
                                                         templateConfiguration.contentPlaceholderFormatSeparator(),
                                                         false);
         }

         projectDocumentBuilder = projectDocumentBuilder.streamContent(replacedContent, contentEncoding);
      }

      return projectDocumentBuilder.build();
   }

   /**
    * Checks if processed document is a candidate for content replacement.
    *
    * @param templateDocument identifier of template document being processed
    * @param projectDocument identifier of project document being processed
    *
    * @return whether processed document is a candidate for content replacement
    */
   private boolean canReplaceContent(DocumentEntry templateDocument, DocumentEntry projectDocument) {
      Optional<Boolean> canReplaceContentForTemplate = templateConfiguration
            .replaceContentFilter()
            .upstreamDocumentEntrySpecification(filterContentTypeSpecification(DEFAULT_REPLACE_CONTENT_TYPES),
                                                null)
            .map(criteria -> criteria.satisfiedBy(templateDocument));
      Optional<Boolean> canReplaceContentForProject = projectConfiguration
            .replaceContentFilter()
            .downstreamDocumentEntrySpecification(filterContentTypeSpecification(DEFAULT_REPLACE_CONTENT_TYPES),
                                                  null)
            .map(criteria -> criteria.satisfiedBy(projectDocument));

      return canReplaceContentForTemplate
            .map(t -> canReplaceContentForProject.map(p -> p && t).orElse(t))
            .or(() -> canReplaceContentForProject)
            .orElse(false);
   }

   /**
    * Generic placeholder replacement function. Replace placeholder, optionally containing a format
    * specification, by matching attribute value. Format specification must be supported, or it will be
    * considered as part of the attribute name.
    *
    * @return placeholder replacement, or {@code null} if no matching attribute found
    */
   private Object placeholderReplacerMapper(DocumentPath documentId,
                                            String placeholder,
                                            String formatSeparator,
                                            boolean toPath) {
      String[] placeholderParts = StringUtils.split(placeholder, formatSeparator);

      if (placeholderParts.length > 0) {
         String attribute = placeholderParts[0];
         Format format = NOOP_FORMAT;

         if (placeholderParts.length > 1) {
            format = nullable(formats.get(placeholderParts[placeholderParts.length - 1]), NOOP_FORMAT);

            attribute = StringUtils.join(placeholderParts,
                                         formatSeparator,
                                         0,
                                         placeholderParts.length - (format != null ? 1 : 0));
         }

         final Format finalFormat = format;
         final String finalAttribute = attribute;

         return projectConfiguration
               .attribute(attribute)
               .map(a -> finalFormat.format(a.value(), toPath))
               .orElseGet(() -> {
                  if (!missingPlaceholderAttributes.contains(finalAttribute)) {
                     log.warn("[processor] {} : Missing '{}' project attribute referenced from placeholder",
                              documentId.stringValue(),
                              finalAttribute);
                     missingPlaceholderAttributes.add(finalAttribute);
                  }
                  return null;
               });
      } else {
         return null;
      }
   }

   /**
    * @param contentTypes content types to filter
    *
    * @implSpec {@code DocumentEntryCriteria::contentType(CriteriaBuilder.in(...)} does not support
    *       MimeType::matches operation.
    */
   private DocumentEntrySpecification filterContentTypeSpecification(List<MimeType> contentTypes) {
      return documentEntry -> {
         MimeType contentType =
               documentEntry.metadata().contentType().orElse(APPLICATION_OCTET_STREAM).strippedParameters();

         return contentTypes.stream().anyMatch(contentType::matches);
      };
   }

   /**
    * Filter document identifier with both attribute inference and placeholders.
    *
    * @param documentId document identifier to filter
    *
    * @return filtered document identifier
    */
   private DocumentPath filterDocumentId(DocumentPath documentId) {
      String documentPath = documentId.stringValue();

      Reader filteredDocumentPath = new StringReader(documentPath);
      filteredDocumentPath = replaceContentInferredAttributes(filteredDocumentPath, true);
      if (!projectConfiguration.inferPlaceholders()) {
         filteredDocumentPath = replaceContentPlaceholders(documentId,
                                                           filteredDocumentPath,
                                                           templateConfiguration.pathPlaceholderBegin(),
                                                           templateConfiguration.pathPlaceholderEnd(),
                                                           templateConfiguration.pathPlaceholderFormatSeparator(),
                                                           true);
      }

      return DocumentPath.of(readerToString(filteredDocumentPath));
   }

   private Reader replaceContentPlaceholders(DocumentPath documentId,
                                             Reader content,
                                             String placeholderBegin,
                                             String placeholderEnd,
                                             String formatSeparator,
                                             boolean toPath) {
      return new PlaceholderReplacerReader(content,
                                           placeholder -> placeholderReplacerMapper(documentId,
                                                                                    placeholder,
                                                                                    formatSeparator,
                                                                                    toPath),
                                           placeholderBegin,
                                           placeholderEnd);
   }

   private String readerToString(Reader reader) {
      try {
         return IOUtils.toString(reader);
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }
   }

   /**
    * Filter content with inferred attributes using all registered formats, and also a case-insensitive
    * format of original attribute value.
    *
    * @param content content to filter
    * @param toPath whether to format attributes for paths
    *
    * @return filtered content
    */
   private Reader replaceContentInferredAttributes(Reader content, boolean toPath) {
      List<TokenReplacer> replacers = list();
      for (Attribute templateAttribute : templateConfiguration.attributes()) {
         stream(formats.values())
               .map(format -> inferredAttributeStringReplacer(toPath, templateAttribute, format, false))
               .forEach(replacers::add);
         replacers.add(inferredAttributeStringReplacer(toPath, templateAttribute, NOOP_FORMAT, true));
      }
      return new GeneralReplacerReader(content, __ -> new MultiReplacer(replacers), REPLACER_BUFFER_SIZE);
   }

   private StringReplacer inferredAttributeStringReplacer(boolean toPath,
                                                          Attribute templateAttribute,
                                                          Format format,
                                                          boolean ignoreCase) {
      String templateAttributeValue = format.format(templateAttribute.value(), toPath);
      String projectAttributeValue = filterContentInferredAttributeValue(templateAttribute, format, toPath);

      return new StringReplacer(templateAttributeValue, projectAttributeValue, ignoreCase);
   }

   private String filterContentInferredAttributeValue(Attribute templateAttribute,
                                                      Format format,
                                                      boolean toPath) {
      if (projectConfiguration.inferPlaceholders()) {
         return attributeToPlaceholder(templateAttribute.name(), format, toPath);
      } else {
         Attribute projectAttribute = projectConfiguration
               .attribute(templateAttribute.name())
               .or(() -> {
                  if (templateAttribute.required()) {
                     return optional();
                  } else {
                     return optional(templateAttribute);
                  }
               })
               .orElseThrow(() -> new IllegalStateException(String.format("Missing '%s' project attribute",
                                                                          templateAttribute.name())));

         return format.format(projectAttribute.value(), toPath);
      }
   }

   /**
    * Generates the placeholder representation for the given attribute and format.
    *
    * @param attributeName attribute name to transform
    * @param format format to identify in transformed placeholder, not that the {@link NoopFormat} is
    *       not represented (no format)
    * @param toPath whether to use the path-like representation
    *
    * @return placeholder representation
    */
   private String attributeToPlaceholder(String attributeName, Format format, boolean toPath) {
      if (toPath) {
         return projectConfiguration.pathPlaceholderBegin()
                + attributeName
                + (format.equals(NOOP_FORMAT)
                   ? ""
                   : projectConfiguration.pathPlaceholderFormatSeparator()
                     + format.name())
                + projectConfiguration.pathPlaceholderEnd();
      } else {
         return projectConfiguration.contentPlaceholderBegin()
                + attributeName
                + (format.equals(NOOP_FORMAT)
                   ? ""
                   : projectConfiguration.contentPlaceholderFormatSeparator()
                     + format.name())
                + projectConfiguration.contentPlaceholderEnd();
      }
   }

}
