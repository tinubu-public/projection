/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import static org.apache.commons.lang3.StringUtils.stripEnd;

import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Projection repository general operations.
 */
public class ProjectionRepository {

   /** Projection directory path relative to a given projection subtree. */
   private static final Path PROJECTION_DIRECTORY = Path.of(".projection");
   /** Projection configuration document path relative to projection directory path. */
   private static final Path PROJECTION_CONFIGURATION = Path.of("config");

   /** Projection directory path relative to a given projection subtree. */
   public Path projectionDirectory() {
      return PROJECTION_DIRECTORY;
   }

   /** Projection configuration document path relative to any projection subtree. */
   public Path projectionConfiguration() {
      return projectionConfiguration(PROJECTION_DIRECTORY);
   }

   /** Projection configuration document path for specified projection path. */
   public Path projectionConfiguration(Path projectionPath) {
      return projectionPath.resolve(PROJECTION_CONFIGURATION);
   }

   /**
    * Detects all paths containing a projection configuration in the specified repository. The returned
    * paths are the parent paths of the projection configurations and are the paths of each sub-projection.
    *
    * @param repository repository to search for projections
    * @param projectionConfiguration projection configuration's relative path to each sub-projection
    * @param primaryProjectionConfiguration whether to match primary projection configuration
    * @param subProjectionConfigurations whether to match sub-projection configurations
    * @param firstSubProjectionOnPath whether to return first sub-projection found on any given path, or
    *       all sub-projections found.
    *
    * @return stream of sub-projection relative paths, can be empty if configuration is the primary projection
    *       configuration.
    */
   public Stream<Path> subProjections(DocumentRepository repository,
                                      Path projectionConfiguration,
                                      boolean primaryProjectionConfiguration,
                                      boolean subProjectionConfigurations,
                                      boolean firstSubProjectionOnPath) {
      Stream<Path> projectionPaths = repository
            .findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                      .documentPath(CriterionBuilder.match(
                                                            projectionConfigurationGlobMatcher(
                                                                  projectionConfiguration,
                                                                  primaryProjectionConfiguration,
                                                                  subProjectionConfigurations)))
                                                      .build())
            .map(e -> {
               Path path = e.documentId().value();
               return Path.of(stripEnd(StringUtils.removeEnd(path.toString(),
                                                             projectionConfiguration.toString()), "/"));
            });

      if (firstSubProjectionOnPath) {
         List<Path> projectionPathsList = list(projectionPaths);
         return stream(projectionPathsList).filter(path -> path.toString().isEmpty() || projectionPathsList
               .stream()
               .noneMatch(p -> !path.equals(p) && path.startsWith(p)));
      }

      return projectionPaths;
   }

   /**
    * Generates a GLOB pattern for the specified projection configuration to be used in
    * {@link CriterionBuilder#match(Comparable)}.
    *
    * @param projectionConfiguration projection configuration relative path
    * @param primaryProjectionConfiguration whether to match primary projection configuration
    * @param subProjectionConfigurations whether to match sub-projection configurations
    *
    * @return GLOB pattern for projection configurations
    *
    * @see FileSystem#getPathMatcher(String)
    */
   public Path projectionConfigurationGlobMatcher(Path projectionConfiguration,
                                                  boolean primaryProjectionConfiguration,
                                                  boolean subProjectionConfigurations) {
      if (primaryProjectionConfiguration) {
         if (subProjectionConfigurations) {
            return Path.of(String.format("glob:{%1$s,**/%1$s}", projectionConfiguration));
         } else {
            return Path.of(String.format("glob:%s", projectionConfiguration));
         }
      } else {
         if (subProjectionConfigurations) {
            return Path.of(String.format("glob:**/%s", projectionConfiguration));
         } else {
            throw new IllegalArgumentException("At least one location must be requested");
         }
      }
   }

}