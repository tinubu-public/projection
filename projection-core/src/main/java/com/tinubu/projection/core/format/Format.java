/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import com.tinubu.projection.core.Attribute;

/**
 * Format specification for attribute values.
 */
public interface Format {

   /** Format name. */
   String name();

   /**
    * Format specified content.
    *
    * @param content content to format
    * @param toPath whether content represents some file content, or a path name. For path names
    *       special rules to match multiple directories are applied. E.g.: {@code com.tinubu.projection} is
    *       formatted to {@code com/tinubu/projection}
    *
    * @return formatted content
    */
   String format(String content, boolean toPath);

   /**
    * Convenient format operation.
    *
    * @param content content to format as an attribute
    * @param toPath whether content represents some file content, or a path name
    *
    * @return formatted content
    */
   default String format(Attribute content, boolean toPath) {
      return format(content.value(), toPath);
   }

}
